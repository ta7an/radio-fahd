import FirebaseDatabase
import NVActivityIndicatorView

let appRedColor = UIColor(red: 111.0/255.0, green: 111.0/255.0, blue: 113.0/255.0, alpha: 1.0)

let databaseRef = Database.database().reference()

//let generalTopic = "TEST"
let generalTopic = "global"

extension Date
{
    static func timeAgoStringFromDate(date: Date) -> String? {
        let formatter = DateComponentsFormatter()
        formatter.unitsStyle = .full
        
        let now = Date()
        
        var calendar = NSCalendar.current
        calendar.locale = Locale(identifier: "ar")
        formatter.calendar = calendar
        
        let components1: Set<Calendar.Component> = [.year, .month, .weekOfMonth, .day, .hour, .minute, .second]
        let components = calendar.dateComponents(components1, from: date, to: now)
        
        if components.year ?? 0 > 0 {
            formatter.allowedUnits = .year
        } else if components.month ?? 0 > 0 {
            formatter.allowedUnits = .month
        } else if components.weekOfMonth ?? 0 > 0 {
            formatter.allowedUnits = .weekOfMonth
        } else if components.day ?? 0 > 0 {
            formatter.allowedUnits = .day
        } else if components.hour ?? 0 > 0 {
            formatter.allowedUnits = [.hour]
        } else if components.minute ?? 0 > 0 {
            formatter.allowedUnits = .minute
        } else {
            formatter.allowedUnits = .second
        }
        let formatString = NSLocalizedString("منذ %@", comment: "Used to say how much time has passed. e.g. '2 hours ago'")
        
        guard let timeString = formatter.string(for: components) else {
            return nil
        }
        return String(format: formatString, timeString)
    }
    

}

extension UIViewController: NVActivityIndicatorViewable
{
    @objc func back()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setUpBackButton()
    {
        let buttonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_back"), style: .plain, target: self, action: #selector(self.back))
        buttonItem.tintColor = .white
        self.navigationItem.leftBarButtonItem  = buttonItem
    }
    
    func showLoading()
    {
//        _ size: CGSize(self.view.frame.size),
        self.startAnimating( message: "جاري التحميل", type: .ballClipRotatePulse, color: appRedColor, padding: 0, displayTimeThreshold: nil, minimumDisplayTime: nil, backgroundColor: UIColor(red: 29.0/255.0, green: 29.0/255.0, blue: 29.0/255.0, alpha: 1.0))
    }
    
    @objc func hideLoading()
    {
        self.stopAnimating()
    }
}

protocol PassTouchesScrollViewDelegate
{
    func touchBegan()
    func touchMoved()
}


class PassTouchesScrollView: UIScrollView {
    
    var delegatePass : PassTouchesScrollViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        // Notify it's delegate about touched
        self.delegatePass?.touchBegan()
        
        if self.isDragging == true {
            self.next?.touchesBegan(touches, with: event)
        } else {
            super.touchesBegan(touches, with: event)
        }
        
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        // Notify it's delegate about touched
        self.delegatePass?.touchMoved()
        
        if self.isDragging == true {
            self.next?.touchesMoved(touches, with: event)
        } else {
            super.touchesMoved(touches, with: event)
        }
    }
}

