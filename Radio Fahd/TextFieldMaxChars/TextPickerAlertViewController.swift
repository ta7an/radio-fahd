//
//  TextPickerAlertViewController.swift
//  Radio Fahd
//
//  Created by Mohamed Altahan on 2/19/18.
//  Copyright © 2018 Mohamed Altahan. All rights reserved.
//

import UIKit
import MaterialTextField
import PopupDialog

class TextPickerAlertViewController: UIViewController {

    var maxLength = 0
    var minLength = 0
    var textPlaceHolder = ""

    @IBOutlet weak var popupTitleLabel: UILabel!
    @IBOutlet weak var textField: MFTextField!
    weak var sendButton: DefaultButton?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.textField.maxLength = self.maxLength
        self.textField.placeholder = self.textPlaceHolder
        // set title label text..
        self.popupTitleLabel.text = self.title
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.textFieldDidChangeValue(self.textField)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    class func showTextInputPopup(inside controller: UIViewController,
                                  maximumLength: Int,
                                  minimumLength: Int,
                                  title: String,
                                  txtPlaceholder: String,
                                  completion: @escaping ((_ text: String) -> Void))
    {
        if let popupController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TextPickerAlertViewController") as? TextPickerAlertViewController
        {
            popupController.maxLength = maximumLength
            popupController.minLength = minimumLength
            popupController.textPlaceHolder = txtPlaceholder
            popupController.title = title
            
            let popup = PopupDialog(viewController: popupController,
                                    buttonAlignment: .horizontal,
                                    preferredWidth: 200)
            
            let buttonOne = DefaultButton(title: "إرسال")
            {
                if let text = popupController.textField.text,
                    text.characters.count > popupController.minLength
                {
                    completion(text)
                }
            }
            buttonOne.setTitleColor(UIColor(red: 9.0/255.0, green: 9.0/255.0, blue: 9.0/255.0, alpha: (1.0)), for: .normal)
            buttonOne.setTitleColor(UIColor(red: 9.0/255.0, green: 9.0/255.0, blue: 9.0/255.0, alpha: (0.4)), for: .disabled)
            popupController.sendButton = buttonOne
            
            // This button will not the dismiss the dialog
            let buttonTwo = CancelButton(title: "إلغاء")
            {
                popup.dismiss()
            }

            popup.addButtons([buttonOne, buttonTwo])
            
            // Present dialog
            controller.present(popup, animated: true, completion: nil)
        }
    }
    
    @IBAction func textFieldDidChangeValue(_ sender: MFTextField)
    {
        let text = self.textField.text ?? ""
        let textLength = text.characters.count
        let err = NSError(domain: "", code: 100, userInfo: [NSLocalizedDescriptionKey: "\(textLength)/\(self.maxLength)"])
        self.textField.setError(err, animated: true)
        
        self.sendButton?.isEnabled = textLength > minLength && textLength < maxLength
    }
    
}
