//
//  AboutAppViewController.swift
//  Radio Fahd
//
//  Created by Mohamed Altahan on 1/22/18.
//  Copyright © 2018 Mohamed Altahan. All rights reserved.
//

import UIKit

class AboutAppViewController: UIViewController {

    @IBOutlet weak var ourWorkButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setUpBackButton()
        self.ourWorkButton.layer.borderWidth = 1.5
        self.ourWorkButton.layer.borderColor = UIColor.white.cgColor
        self.ourWorkButton.layer.cornerRadius = 4

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func openLink(_ sender: UIButton)
    {
        if let url = URL(string: "http://www.fahadalkubaisi.com"),
            UIApplication.shared.canOpenURL(url)
        {
            UIApplication.shared.open(url)
        }
    }
    
    @IBAction func openMail(_ sender: UIButton)
    {
        let email = "dndnah.com@gmail.com"
        if let url = URL(string: "mailto:\(email)")
        {
            UIApplication.shared.open(url)
        }
    }
    
    @IBAction func openTwitter(_ sender: UIButton)
    {
        //
        
        if let url = URL(string: "twitter:///user?screen_name=FahadAlkubaisi"),
            UIApplication.shared.canOpenURL(url)
        {
            UIApplication.shared.open(url)
        } else if let url = URL(string: "http://twitter.com/FahadAlkubaisi?lang=ar"),
            UIApplication.shared.canOpenURL(url)
        {
            UIApplication.shared.open(url)
        }
    }
    
    @IBAction func openFacebook(_ sender: UIButton)
    {
        if let url = URL(string: "fb://profile/1586261831618146"),
            UIApplication.shared.canOpenURL(url)
        {
            UIApplication.shared.open(url)
        } else if let url = URL(string: "https://m.facebook.com/FahadAlKubaisii"),
            UIApplication.shared.canOpenURL(url)
        {
            UIApplication.shared.open(url)
        }
    }
    
    @IBAction func openInstagram(_ sender: UIButton)
    {
        if let url = URL(string: "instagram://user?username=AlKubaisiOfficial"),
            UIApplication.shared.canOpenURL(url)
        {
            UIApplication.shared.open(url)
        } else if let url = URL(string: "https://www.instagram.com/AlKubaisiOfficial/"),
            UIApplication.shared.canOpenURL(url)
        {
            UIApplication.shared.open(url)
        }
    }
    
    @IBAction func openYoutube(_ sender: UIButton)
    {
        let youtubeId = "UCclxzDcDgvYeZNu3-uttJ1Q"
        var url = URL(string:"youtube://\(youtubeId)")!
        if !UIApplication.shared.canOpenURL(url)  {
            url = URL(string:"http://www.youtube.com/watch?v=\(youtubeId)")!
        }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    @IBAction func openSnap(_ sender: UIButton)
    {
        let ID = "kubaisiofficial"
        var url = URL(string:"snapchat://add/\(ID)")!
        if !UIApplication.shared.canOpenURL(url)  {
            url = URL(string:"https://snapchat.com/add/\(ID)")!
        }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    @IBAction func openItuness(_ sender: UIButton)
    {
        let urlStr = "https://itunes.apple.com/us/artist/fahad-al-kubaisi/642501742"
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(URL(string: urlStr)!, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(URL(string: urlStr)!)
        }
    }
    
    @IBAction func openOurWorkClicked(_ sender: UIButton)
    {
        let urlStr = "http://www.alfarisprojects.com/works/"
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(URL(string: urlStr)!, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(URL(string: urlStr)!)
        }
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
