//
//  MakeAdViewController.swift
//  Radio Fahd
//
//  Created by Mohamed Altahan on 1/27/18.
//  Copyright © 2018 Mohamed Altahan. All rights reserved.
//

import UIKit
import DLRadioButton
import Alamofire
import MaterialTextField
import SwiftValidator
//import IHKeyboardAvoiding

class MakeAdViewController: UIViewController, ValidationDelegate
{
    let validator = Validator()
    
    @IBOutlet weak var adOnTopButton: DLRadioButton!
    @IBOutlet weak var bigAdInApp: DLRadioButton!

    @IBOutlet weak var titleTF: MFTextField!
    
    @IBOutlet weak var testTF: MFTextField!
    
    @IBOutlet weak var imageURLTF: MFTextField!
    
    @IBOutlet weak var linkToOpenURLTF: MFTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setUpBackButton()
//        KeyboardAvoiding.avoidingView = self.view
        
        validator.registerField(titleTF, rules: [RequiredRule()])
        validator.registerField(testTF, rules: [RequiredRule()])
        validator.registerField(imageURLTF, rules: [RequiredRule()])
        validator.registerField(linkToOpenURLTF, rules: [RequiredRule()])
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func sendAdAction(_ sender: UIButton)
    {
        self.validator.validate(self)
    }
    
    func validationFailed(_ errors: [(Validatable, ValidationError)]) {
        
    }
    
    func validationSuccessful()
    {
        if let selectedbtn = self.adOnTopButton.selected()
        {
            var selectedType = ""
            if selectedbtn == self.adOnTopButton
            {
                selectedType = "nI"
            } else {
                selectedType = "ia"
            }
            
            let URL = "https://fcm.googleapis.com/fcm/send"
            let headers: HTTPHeaders = ["Authorization": "key=AAAA7EBr7IE:APA91bFzTdcUixpqIW5SnGYayzhAEXDEhWq34ncs_GuEoWGpQuW7B-KUj53qsYArbEW9ngMw_87xc47Ooz_mDUwKug7nN95DtVGWQrN9kA1MvhgToeF4RSQbovVsQJQh1ynj2olDn638aD8InUaS13cKDbAX-pWGXg",
                                        "Content-Type": "application/json"]
            
            let data: [String: String] = ["type": selectedType,
                                          "message":testTF.text!,
                                          "image": imageURLTF.text!,
                                          "url":linkToOpenURLTF.text!,
                                          "title": self.titleTF.text!
            ]
            let notification: [String: String] = ["title": self.titleTF.text!]
            
            let parameters: Parameters = [
                "to": "/topics/\(generalTopic)",
                "data": data,
                "notification": notification
            ]
            
            self.showLoading()
            Alamofire.request( URL, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON(completionHandler: { (response) in
                self.hideLoading()
                PopUpAlert.sharedInstance.showAlert(context: self, title: "", message: "تم الإرسال بنجاح", dismissAction: {
                    self.navigationController?.popViewController(animated: true)
                })
            })
            
            
        } else {
            // No selecttion.
            PopUpAlert.sharedInstance.showAlert(context: self, title: "خطأ", message: "يجب ملئ البيانات واختيار نوع الاعلان.")
        }
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        super.touchesBegan(touches, with: event)
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
