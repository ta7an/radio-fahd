//
//  ChatSettingsViewController.swift
//  Radio Fahd
//
//  Created by Mohamed Altahan on 1/26/18.
//  Copyright © 2018 Mohamed Altahan. All rights reserved.
//

import UIKit
import JTMaterialSwitch
import FirebaseDatabase

class ChatSettingsViewController: UIViewController, JTMaterialSwitchDelegate
{
    
    @IBOutlet weak var switchLBL: UILabel!
    var state: JTMaterialSwitchState = JTMaterialSwitchStateOff

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let switchControl = JTMaterialSwitch()
        switchControl?.delegate = self
        switchControl?.thumbOnTintColor = appRedColor
        switchControl?.trackOnTintColor = appRedColor.withAlphaComponent(0.4)
        switchControl?.rippleFillColor = appRedColor.withAlphaComponent(0.3)
        switchControl?.center = CGPoint(x: 72, y: self.switchLBL.center.y)
        self.view.addSubview(switchControl!)
        self.setUpBackButton()
        
        databaseRef.child("GeneralSettings/chatWork").observeSingleEvent(of: .value) { (snapshot) in
            if let obj = snapshot.value as? Bool
            {
                switchControl?.setOn(obj, animated: false)
            }
        }
    }
    
    func switchStateChanged(_ currentState: JTMaterialSwitchState)
    {
        state = currentState
        switchLBL.text = state == JTMaterialSwitchStateOn ? "الشات يعمل" : "الشات لا يعمل"
    }
    
    @IBAction func save()
    {
        let chatWorking = state == JTMaterialSwitchStateOn
        databaseRef.child("GeneralSettings/chatWork").setValue(chatWorking, withCompletionBlock: { (error, dbRef) in
            PopUpAlert.sharedInstance.showAlert(context: self, title: "تم الحفظ", message: "تم الحفظ بنجاح", dismissAction: {
                self.navigationController?.popViewController(animated: true)
            })
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
