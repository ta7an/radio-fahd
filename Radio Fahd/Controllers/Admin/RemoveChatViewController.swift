//
//  RemoveChatViewController.swift
//  Radio Fahd
//
//  Created by Mohamed Altahan on 1/27/18.
//  Copyright © 2018 Mohamed Altahan. All rights reserved.
//

import UIKit
import FSCalendar

class RemoveChatViewController: UIViewController, FSCalendarDelegate, FSCalendarDataSource
{

    @IBOutlet weak var calendarView: FSCalendar!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpBackButton()

        
//        self.calendarView.formatter = Formatter()
    }
    @IBAction func back(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func remove(_ sender: UIButton)
    {
        
        
        if let date = self.calendarView.selectedDate
        {
            let interval = date.timeIntervalSince1970
            
            let firebaseTime: Double = Double(interval * 1000.0)
            self.showLoading()
            databaseRef.child("chatmodel").queryOrdered(byChild: "timeStamp").observeSingleEvent(of: .value, with: { (snapshot) in
                if let arr = snapshot.value as? [String: [String: Any]]
                {
                    
                    var keysToDelete: [String: Any] = [:]
                    for (key, object) in arr
                    {

                        if let time = object["timeStamp"] as? String,
                            let timeInt = Double(time),
                            firebaseTime > timeInt
                        {
                            keysToDelete[key] = NSNull()
                            //                            databaseRef.child("chatmodel/\(key)").removeValue(completionBlock: { (error, ref) in
//                            })
                        }
//
                        //                    snapshot.ref.removeValue()
                    }
                    
                    if keysToDelete.keys.count > 0
                    {
                        databaseRef.child("chatmodel").updateChildValues(keysToDelete, withCompletionBlock: { (error, ref) in
                            
                            self.hideLoading()
                            if error == nil
                            {
                                self.showSuccessPopup()
                            } else {
                                self.showFailurePopup()
                            }
                        })
                    } else {
                        self.hideLoading()
                        PopUpAlert.sharedInstance.showAlert(context: self, title: "", message: "لا توجد رسائل قبل هذا التاريخ")
                    }
                    
                }
            })
            //queryEnding(atValue: firebaseTime)
            //            .queryLimited(toLast: 1)

//            print("-----\() ------")
            
            
        } else {
            PopUpAlert.sharedInstance.showAlert(context: self, title: "", message: "يجب تحديد التاريخ الذي سيتم حذف الرسائل السابقة له٫")
        }
        
    }
    
    @objc func showSuccessPopup()
    {
        PopUpAlert.sharedInstance.showAlert(context: self, title: "", message: "تم الحذف بنجاح٫")
    }
    
    @objc func showFailurePopup()
    {
        PopUpAlert.sharedInstance.showAlert(context: self, title: "", message: "حدث خطأ ما، يرجى المحاولة مرة أخرى")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
