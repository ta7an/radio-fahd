//
//  PrivateChatViewController.swift
//  Radio Fahd
//
//  Created by Mohamed Altahan on 1/31/18.
//  Copyright © 2018 Mohamed Altahan. All rights reserved.
//

import UIKit
import Firebase
import MaterialTextField
//import IHKeyboardAvoiding
import SDWebImage

class PrivateChatViewController: UIViewController, UITableViewDataSource, UITableViewDelegate,
UITextFieldDelegate, UINavigationControllerDelegate
{

    @IBOutlet weak var chatTableView: UITableView!
    @IBOutlet weak var textField: MFTextField!
    @IBOutlet weak var sendButton: UIButton!
    var ref: DatabaseReference!
    var messages: [DataSnapshot]! = []
    var msglength: NSNumber = 1000
    fileprivate var _refHandle: DatabaseHandle?
    var userInfoObj: [String : String]?
    var userID: String? = Auth.auth().currentUser?.uid
    let chatCellSettings = ChatCellSettings.getInstance()
    var navTitle = "الرسائل الخاصة"
    
    static func makeGeneralChatViewController() -> GeneralChatViewController
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyboard.instantiateViewController(withIdentifier: "GeneralChatViewController") as! GeneralChatViewController
        return newViewController
    }

    override func viewDidLoad() {
        super.viewDidLoad()
//        KeyboardAvoiding.avoidingView = self.view
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: .UIKeyboardWillShow, object: nil)

        self.chatTableView.register(UITableViewCell.self, forCellReuseIdentifier: "tableViewCell")
        
        configureDatabase()
        
        self.chatTableView.estimatedRowHeight = 60
        self.chatTableView.rowHeight = UITableViewAutomaticDimension
        
        chatCellSettings?.setSenderBubbleColorHex("F02032")
        chatCellSettings?.setReceiverBubbleColorHex("FFFFFF")
        chatCellSettings?.setSenderBubbleNameTextColorHex("FFFFFF")
        chatCellSettings?.setReceiverBubbleNameTextColorHex("FFFFFF")
        chatCellSettings?.setSenderBubbleMessageTextColorHex("FFFFFF")
        chatCellSettings?.setReceiverBubbleMessageTextColorHex("000000")
        chatCellSettings?.setSenderBubbleTimeTextColorHex("FFFFFF")
        chatCellSettings?.setReceiverBubbleTimeTextColorHex("FFFFFF")
        chatCellSettings?.setSenderBubbleFontWithSizeForName(UIFont.boldSystemFont(ofSize: 11))
        chatCellSettings?.setReceiverBubbleFontWithSizeForName(UIFont.boldSystemFont(ofSize: 11))
        chatCellSettings?.setSenderBubbleFontWithSizeForMessage(UIFont.systemFont(ofSize: 14))
        chatCellSettings?.setReceiverBubbleFontWithSizeForMessage(UIFont.systemFont(ofSize: 14))
        chatCellSettings?.setSenderBubbleFontWithSizeForTime(UIFont.systemFont(ofSize: 11))
        
        //        self.chatTableView.register(ChatTableViewCell.self, forCellReuseIdentifier: "chatSend")
        //        self.chatTableView.register(ChatTableViewCell.self, forCellReuseIdentifier: "chatReceive")
        
        //  Converted to Swift 4 by Swiftify v1.0.6577 - https://objectivec2swift.com/
        var nib = UINib(nibName: "ChatSendCell", bundle: nil)
        self.chatTableView.register(nib, forCellReuseIdentifier: "chatSend")
        nib = UINib(nibName: "ChatReceiveCell", bundle: nil)
        self.chatTableView.register(nib, forCellReuseIdentifier: "chatReceive")
        self.setUpBackButton()
        self.navigationItem.title = navTitle
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    // MARK: Chat
    deinit {
        if let refHandle = _refHandle,
            let ID = userID
        {
            self.ref.child("usersChat/\(ID)").removeObserver(withHandle: refHandle)
        }
        
        NotificationCenter.default.removeObserver(self)
    }
    
    func configureDatabase() {
        ref = Database.database().reference()
        // Listen for new messages in the Firebase database
        if let ID = userID
        {
            _refHandle = self.ref.child("usersChat/\(ID)").observe(.childAdded, with: { [weak self] (snapshot) -> Void in
                guard let strongSelf = self else { return }
                strongSelf.messages.append(snapshot)
                strongSelf.chatTableView.insertRows(at: [IndexPath(row: strongSelf.messages.count-1, section: 0)], with: .automatic)
                strongSelf.chatTableView.scrollToRow(at: IndexPath(item:strongSelf.messages.count-1, section: 0), at: .bottom, animated: true)
            })

            databaseRef.child("Users/\(ID)").observeSingleEvent(of: .value, with: { (snapshot) in
                if let obj = snapshot.value as? [String: Any]
                {
                    self.userInfoObj = [:]
                    self.userInfoObj!["id"] = obj["user_id"] as? String ?? ""
                    self.userInfoObj!["name"] = obj["name"] as? String ?? ""
                    self.userInfoObj!["photo_profile"] = obj["photo_profile"] as? String ?? ""
                }
            })
        }
    }
    
    // UITextViewDelegate protocol methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let text = textField.text else { return true }
        textField.text = ""
        view.endEditing(true)
        
        let millisecondsSince1970 = Date().timeIntervalSince1970 * 1000
        let data = ["message": text, "timeStamp": "\(millisecondsSince1970)" ] as [String : Any]
        sendMessage(withData: data)
        return true
    }
    
    func sendMessage(withData data: [String: Any])
    {
        if self.userInfoObj != nil,
            let ID = userID
        {
            var mdata = data
            let createdObj = self.ref.child("usersChat/\(ID)").childByAutoId()
            mdata["id"] = createdObj.key
            mdata["userModel"] = self.userInfoObj!
            createdObj.setValue(mdata)
        }
    }
    
    @IBAction func didSendMessage(_ sender: UIButton) {
        _ = textFieldShouldReturn(textField)
    }
    
    func inviteFinished(withInvitations invitationIds: [String], error: Error?) {
        if let error = error {
            print("Failed: \(error.localizedDescription)")
        } else {
            print("Invitations sent")
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        
        let newLength = text.characters.count + string.characters.count - range.length
        return newLength <= self.msglength.intValue // Bool
    }
    
    // UITableViewDataSource protocol methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let messageSnapshot: DataSnapshot! = self.messages[indexPath.row]
        guard let message = messageSnapshot.value as? [String:Any] else { return self.chatTableView.dequeueReusableCell(withIdentifier: "chatReceive", for: indexPath) }
        
        var isSender = false
        if let UID = userID,
            let fetchedID = ((message["userModel"] as? [String:String])?["id"]),
            UID == fetchedID
        {
            isSender = true
        }
        let ID = isSender ? "chatSend" : "chatReceive"
        let cell = self.chatTableView.dequeueReusableCell(withIdentifier: ID, for: indexPath) as! ChatTableViewCellXIB
        cell.backgroundColor = .clear
        cell.chatNameLabel.text = ((message["userModel"] as? [String:String])?["name"]) ?? ""
        
        cell.chatMessageLabel.text = (message["message"] as? String) ?? ""
        var  timeDouble: Double?

        if let timeStr = message["timeStamp"] as? String
        {
            timeDouble = Double(timeStr)
        } else if let timeDBL = message["timeStamp"] as? Double
        {
            timeDouble = timeDBL
        }
        
        if timeDouble != nil
        {
            let date = Date(timeIntervalSince1970: timeDouble! / 1000.0)
            let str = Date.timeAgoStringFromDate(date: date)
            cell.chatTimeLabel.text = str
        }
        if let path = ((message["userModel"] as? [String:String])?["photo_profile"]),
            let url = URL(string: path)
        {
            cell.chatUserImage.sd_setImage(with: url)
        } else {
            cell.chatUserImage.image = UIImage(named: "splash_logo")
        }
        (cell.triangleView as! TriangleView).fillColor = isSender ? appRedColor : UIColor.white
        cell.triangleView.setNeedsDisplay()
        
        //        if let photoURL = (message["userModel"] as? [String:String])?["photo_profile"],
        //        let URL = URL(string: photoURL),
        //            let data = try? Data(contentsOf: URL) {
        //            cell.imageView?.image = UIImage(data: data)
        //        }
        return cell
    }
    
    func showAlert(withTitle title: String, message: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title,
                                          message: message, preferredStyle: .alert)
            let dismissAction = UIAlertAction(title: "تم", style: .destructive, handler: nil)
            alert.addAction(dismissAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
}

extension PrivateChatViewController: UITableViewDataSourcePrefetching {
    public func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        
        
        let urls = indexPaths.map({ (indexPath) -> URL? in
            let messageSnapshot: DataSnapshot! = self.messages[indexPath.row]
            guard let message = messageSnapshot.value as? [String:Any] else {return nil}
            
            if let path = ((message["userModel"] as? [String:String])?["photo_profile"]),
                let url = URL(string: path)
            {
                return url
            }
            
            return nil
            
        })
        SDWebImagePrefetcher.shared().prefetchURLs(urls)
    }
    
    @objc func keyboardWillShow() {
        self.chatTableView.isScrollEnabled = true
//        (self.view.superview as! UIScrollView).isScrollEnabled = true
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.chatTableView.isScrollEnabled = false
//        (self.view.superview as! UIScrollView).isScrollEnabled = false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.chatTableView.isScrollEnabled = true
//        (self.view.superview as! UIScrollView).isScrollEnabled = true
    }

}

