//
//  ViewController.swift
//  Radio Fahd
//
//  Created by Mohamed Altahan on 3/14/17.
//  Copyright © 2017 Mohamed Altahan. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import FirebaseAuth

class SplashAnimationViewController: UIViewController {

    //MARK: Outlets
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var activityIndicatorView: NVActivityIndicatorView!
    var timer: Timer?
    
    //MARK: Methods
    override func viewDidLoad()
    {
        super.viewDidLoad()

        self.fadeOut()
        self.activityIndicatorView.startAnimating()
        self.perform(#selector(self.navigate), with: nil, afterDelay: 3.0)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @objc func navigate()
    {
        if let nav = self.navigationController
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            if Auth.auth().currentUser != nil
            {
                let sideMenuController = storyboard.instantiateViewController(withIdentifier: "SlideMenuMainViewController") as! SlideMenuMainViewController
                nav.setViewControllers([sideMenuController], animated: true)
                
//                    databaseRef.child("GeneralSettings").observeSingleEvent(of: .value, with: { (snapshot) in
//                        if let obj = snapshot.value as? [String: String],
//                            let msg = obj["welcomeMessageGeneral"]
//                        {
//                            PopUpAlert.sharedInstance.showAlert(context: UIApplication.shared.keyWindow!.rootViewController!, title: "", message: msg )
//                        }
//                    })


            } else {
                
                let loginIntroVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginIntroViewController")
                nav.setViewControllers([loginIntroVC!], animated: true)
            }
        }
    }

    func fadeIn(duration: TimeInterval = 1.0)
    {
        UIView.animate(withDuration: duration, animations: {
            self.logoImageView.alpha = 1.0
        }) { (finished) in
            if finished
            {
                self.fadeOut()
            }
        }
    }
    
    /**
     Fade out a view with a duration
     
     - parameter duration: custom animation duration
     */
    func fadeOut(duration: TimeInterval = 1.0)
    {
        UIView.animate(withDuration: duration, animations: {
            self.logoImageView.alpha = 0.5
        }) { (finished) in
            if finished
            {
                self.fadeIn()
            }
        }
    }

}

