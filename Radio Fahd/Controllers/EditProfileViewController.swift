//
//  EditProfileViewController.swift
//  Radio Fahd
//
//  Created by Mohamed Altahan on 1/25/18.
//  Copyright © 2018 Mohamed Altahan. All rights reserved.
//

import UIKit
import MaterialTextField
import SwiftValidator
import FirebaseAuth
//import IHKeyboardAvoiding
import CountryPicker
import SDWebImage
import FirebaseStorage
import FirebaseMessaging
import Firebase

class EditProfileViewController: UIViewController, UITextFieldDelegate, ValidationDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate
{

    @IBOutlet var textFields: [MFTextField]!
    var alertController: UIAlertController?
    let imagePicker: UIImagePickerController = UIImagePickerController()
    var imageChanged = false
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var scrollView: PassTouchesScrollView!
    @IBOutlet weak var nameTF: MFTextField!
    @IBOutlet weak var emailTF: MFTextField!
    @IBOutlet weak var countryTF: MFTextField!
    @IBOutlet weak var ageTF: MFTextField!
    @IBOutlet weak var passwordTF: MFTextField!

    
    let validator = Validator()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        KeyboardAvoiding.avoidingView = self.view
        self.setUpBackButton()
        self.scrollView.delegatePass = self
        // FullNameRule(message: "اكتب الاسم بالكامل")
        validator.registerField(nameTF, rules: [RequiredRule(message: "يجب ملء الاسم")])
        validator.registerField(emailTF, rules: [RequiredRule(message: "يجب إدخال الإيميل"), EmailRule(message: "ايميل خاطئ")])
//        validator.registerField(ageTF, rules: [RequiredRule(message: "يجب إدخال السن")])
//        validator.registerField(countryTF, rules: [RequiredRule(message: "يجب إدخال الدولة")])
        
        if let ID = Auth.auth().currentUser?.uid
        {
            databaseRef.child("Users/\(ID)").observeSingleEvent(of: .value, with: { (snapshot) in
                if let obj = snapshot.value as? [String: Any]
                {
                    if let name = obj["name"] as? String
                    {
                        self.nameTF.text = name
                    }
                    
                    if let country = obj["country"] as? String
                    {
                        self.countryTF.text = country
                    }
                    
                    if let age = obj["age"] as? String
                    {
                        self.ageTF.text = age
                    }
                    
                    if let email = obj["email"] as? String
                    {
                        self.emailTF.text = email
                    }
                    
                    if let img = obj["profile_picture"] as? String,
                        let url = URL(string: img)
                    {
                        self.profileImage.sd_setImage(with: url)
                    }
                    
                }
            })
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        super.touchesBegan(touches, with: event)
    }
    
    func chooseCountry()
    {
        alertController = UIAlertController(title: "اختيار الدولة", message: "\n\n\n\n\n\n\n\n\n\n\n\n", preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let margin:CGFloat = 8.0
        let rect = CGRect(x: margin, y: 0, width: alertController!.view.bounds.size.width - margin * 4.0, height: 240)
        let pickerView = CountryPicker(frame: rect)
        pickerView.delegate = self
        alertController?.view.addSubview(pickerView)
        
        let cancelAction = UIAlertAction(title: "إلغاء", style: .cancel, handler: {(alert: UIAlertAction!) in self.view.endEditing(true)})
        
        alertController?.addAction(cancelAction)
        
        DispatchQueue.main.async {
            self.present(self.alertController!, animated: true, completion:{})
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        if textField == self.countryTF
        {
//            if textField.text != nil && textField.text!.characters.count > 0
//            {
//                return true
//            }
            self.chooseCountry()
            return false
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        validator.validateField(textField){ error in
            if error == nil {
                
            } else {
                
            }
        }
        return true
    }
    
    
    @IBAction func createAccount(_ sender: AnyObject) {
        validator.validate(self)
        
    }
    
    func validationSuccessful()
    {
        var editFinish = false
        var editPassFinish = false
        
        self.showLoading()
        
        for field in self.textFields {
            if let field = field as? MFTextField {
                let err = NSError(domain: "", code: 100, userInfo: [NSLocalizedDescriptionKey: ""])
                field.setError(err as! Error, animated: true)
            }
//            field.error.text = error.errorMessage // works if you added labels
//            error.errorLabel?.isHidden = false
        }

        
        if let email = emailTF.text
        {
            let data: [String: Any] = ["\(Auth.auth().currentUser!.uid)":
                ["name":self.nameTF.text!,
                 "FBM_Token": InstanceID.instanceID().token() ?? "",
                 "country": self.countryTF.text ?? "",
                 "user_id": Auth.auth().currentUser!.uid,
                 "age": self.ageTF.text ?? ""]
            ]
            databaseRef.child("Users").updateChildValues(data, withCompletionBlock:{ (error, dbRef) in
                editFinish = true
                if let img = self.profileImage.image,
                    self.imageChanged,
                    let ID = Auth.auth().currentUser?.uid
                {
                    let data = UIImageJPEGRepresentation(img, 0.8)
                    let meta = StorageMetadata()
                    meta.contentType = "image/jpeg"
                    
                    Storage.storage().reference().child("UsersPictures/\(ID).jpeg").putData(data!, metadata: meta, completion: { (meta, error) in
//                        profile_picture
                        if let profileImageURL = meta?.downloadURL()?.absoluteString
                        {
                            databaseRef.child("Users/\(ID)/profile_picture").setValue("\(profileImageURL)")
                        }

                        if (nil == self.passwordTF.text || editPassFinish)
                        {
                            self.hideLoading()
                            self.navigationController?.popViewController(animated: true)
                        }
                        if nil != error
                        {
                            
                        }
                    })
                } else {
                    self.hideLoading()
                    self.navigationController?.popViewController(animated: true)
                }
            })
        }
        
        if let password = self.passwordTF.text
        {
            editPassFinish = true
            Auth.auth().currentUser?.updatePassword(to: password, completion: { (error) in
                if editFinish
                {
                    self.hideLoading()
                    self.navigationController?.popViewController(animated: true)
                }
            })
        }

    }
    
    func validationFailed(_ errors: [(Validatable, ValidationError)]) {
        // turn the fields to red
        for (field, error) in errors {
            if let field = field as? MFTextField {
//                field.layer.borderColor = UIColor.red.cgColor
//                field.layer.borderWidth = 1.0
                
                let err = NSError(domain: error.errorMessage, code: 100, userInfo: [NSLocalizedDescriptionKey: error.errorMessage])
                field.setError(err as! Error, animated: true)

            }
            error.errorLabel?.text = error.errorMessage // works if you added labels
            error.errorLabel?.isHidden = false
        }
    }

    @IBAction func back(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func pickNewProfileImage(_ sender: UIButton)
    {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum)
        {
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum;
            imagePicker.allowsEditing = false
            
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage
        {
            imageChanged = true

            self.profileImage.image = pickedImage
        }

        self.dismiss(animated: true, completion: { () -> Void in
            
        })
        
    }

    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension EditProfileViewController: CountryPickerDelegate, PassTouchesScrollViewDelegate
{
    func countryPicker(_ picker: CountryPicker!, didSelectCountryWithName name: String!, code: String!)
    {
        self.countryTF.text = name
        self.alertController?.dismiss(animated: true, completion: nil)
    }
    
    func touchBegan()
    {
        self.view.endEditing(true)
    }
    func touchMoved() {}
}
