//
//  SettingsViewController.swift
//  Radio Fahd
//
//  Created by Mohamed Altahan on 1/22/18.
//  Copyright © 2018 Mohamed Altahan. All rights reserved.
//

import UIKit
import JTMaterialSwitch
import FirebaseMessaging

class SettingsViewController: UIViewController, JTMaterialSwitchDelegate
{

    @IBOutlet weak var switchLBL: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        let switchControl = JTMaterialSwitch()
        switchControl?.delegate = self
        switchControl?.thumbOnTintColor = appRedColor
        switchControl?.trackOnTintColor = appRedColor.withAlphaComponent(0.4)
        switchControl?.rippleFillColor = appRedColor.withAlphaComponent(0.3)
        switchControl?.center = CGPoint(x: 72, y: self.switchLBL.center.y)
        self.view.addSubview(switchControl!)
        self.setUpBackButton()
        switchControl?.isOn = UserDefaults.standard.bool(forKey: "NotificationsOn")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func switchStateChanged(_ currentState: JTMaterialSwitchState)
    {
        Messaging.messaging().shouldEstablishDirectChannel = currentState == JTMaterialSwitchStateOn
        UserDefaults.standard.set( (currentState == JTMaterialSwitchStateOn), forKey: "NotificationsOn")

    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
