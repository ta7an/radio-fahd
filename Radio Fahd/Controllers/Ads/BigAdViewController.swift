//
//  BigAdViewController.swift
//  Radio Fahd
//
//  Created by Mohamed Altahan on 2/5/18.
//  Copyright © 2018 Mohamed Altahan. All rights reserved.
//

import UIKit
import SDWebImage

class BigAdViewController: UIViewController {

    @IBOutlet weak var adImageView: UIImageView!
    
    var imagePath: String?
    var adPath: String?

    override func viewDidLoad() {
        super.viewDidLoad()

        if let path = self.imagePath,
            let url = URL(string: path)
        {
            self.adImageView.sd_setImage(with: url)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func dismiss(_ sender: UIButton)
    {
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
    }
    
    @IBAction func imageTapped(_ sender: UITapGestureRecognizer) {
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
        
        
        if self.adPath != nil,
            let url = URL(string: self.adPath!),
            UIApplication.shared.canOpenURL(url)
        {
            UIApplication.shared.openURL(url)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
