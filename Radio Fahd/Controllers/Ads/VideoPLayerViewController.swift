//
//  VideoPLayerViewController.swift
//  Radio Fahd
//
//  Created by Mohamed Altahan on 3/14/18.
//  Copyright © 2018 Mohamed Altahan. All rights reserved.
//

import UIKit
import youtube_ios_player_helper

class VideoPLayerViewController: UIViewController, YTPlayerViewDelegate
{
    var videoID: String?
    @IBOutlet weak var playerView: YTPlayerView!
    var wasRedioPlayingBeforeShowVideo = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.wasRedioPlayingBeforeShowVideo = (UIApplication.shared.delegate as! AppDelegate).audioPlayer.isPlaying
        self.embedYouTube()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func embedYouTube()
    {
        if let vidID = self.videoID
        {
//            let embedHTML = "<iframe width=\"300\" height=\"250\" src=\"http://www.youtube.com/embed/\(vidID)\" frameborder=\"0\" allowfullscreen></iframe>"
//            let html: String = embedHTML
//            webView.loadHTMLString(html, baseURL: nil)
//            view.addSubview(webView)
            self.playerView.load(withVideoId: vidID)
            self.playerView.delegate = self
        }
    }
    
    @IBAction func dismiss(_ sender: UIButton)
    {
        self.playerView.stopVideo()
        if wasRedioPlayingBeforeShowVideo
        {
            (UIApplication.shared.delegate as! AppDelegate).audioPlayer?.play()
        }
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
    }
    
    func playerView(_ playerView: YTPlayerView, didChangeTo state: YTPlayerState) {
        switch state
        {
        case .ended:
            if wasRedioPlayingBeforeShowVideo
            {
                (UIApplication.shared.delegate as! AppDelegate).audioPlayer?.play()
            }
        case .playing:
            (UIApplication.shared.delegate as! AppDelegate).audioPlayer?.pause()
        default:
            break
        }
    }
}
