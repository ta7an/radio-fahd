//
//  MenuViewController.swift
//  KuwaytNews
//
//  Created by Mohamed Altahan on 4/6/16.
//  Copyright © 2016 Plexable. All rights reserved.
//

import UIKit
import FirebaseMessaging
import FirebaseDatabase
import FirebaseAuth
import SDWebImage
import Alamofire

import FacebookLogin
import FacebookCore
import TwitterKit

class MenuViewController: UIViewController {
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
//    @IBOutlet weak var mobileNumber: UILabel!
    
    @IBOutlet var menuButtons: [UIButton]!
    @IBOutlet var usersOnlyMenuButtons: [UIButton]!
    @IBOutlet var adminsOnlyMenuButtons: [UIButton]!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.showUserItemsOnly()
        self.profileImage.layer.cornerRadius =  60
    }
    
    func fillUserData()
    {
//        userName.text = UserAPIManager.sharedInstance.userData?.name ?? ""
//        mobileNumber.text = UserAPIManager.sharedInstance.userData?.mobile ?? ""
    }
    
    fileprivate var _refHandle: DatabaseHandle?
    deinit {
        if let refHandle = _refHandle,
            let ID = Auth.auth().currentUser?.uid
        {
            databaseRef.child("Users/\(ID)/block").removeObserver(withHandle: refHandle)
        }
    }

    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)

        for button in menuButtons
        {
            button.imageView?.contentMode = .scaleAspectFit
        }
        
        self.updateUserDataInView()

        self.showLoading()
        if let ID = Auth.auth().currentUser?.uid
        {
            _refHandle = databaseRef.child("Users/\(ID)").observe(.value, with: {  [weak self] (snapshot) -> Void in
                guard let strongSelf = self else { return }
                
                if let obj = snapshot.value as? [String: Any]
                {
                    if let isBlocked = obj["block"] as? String
                    {
                        if isBlocked == "true"
                        {
                            let messageAlert = UIAlertController(title: "", message: "نأسف لحظر حسابك", preferredStyle: .alert)
                            messageAlert.addAction(UIAlertAction(title: "تم", style: .cancel, handler: {
                                alert -> Void in
                                let loginIntroVC = strongSelf.storyboard?.instantiateViewController(withIdentifier: "LoginIntroViewController")
                                (UIApplication.shared.keyWindow?.rootViewController as? UINavigationController)?.setViewControllers([loginIntroVC!], animated: true)
                            }))
                            UIApplication.shared.keyWindow?.rootViewController?.present(messageAlert, animated: true, completion: nil)
                            
                        }
                    }
                    
                    strongSelf.hideLoading()
                    if let isAdmin = obj["isAdmin"] as? Bool
                    {
                        let nav: UINavigationController? = UIApplication.shared.delegate!.window!!.rootViewController as? UINavigationController;
                        let viewController: SlideMenuMainViewController? = nav?.viewControllers[0] as? SlideMenuMainViewController
                        viewController?.isAdmin = isAdmin
                        
                        if isAdmin
                        {
                            strongSelf.showAdminItemsOnly()
                        } else {
                            strongSelf.showUserItemsOnly()
                        }
                    }
                    databaseRef.child("GeneralSettings").observeSingleEvent(of: .value, with: { (snapshot) in
                        if let obj = snapshot.value as? [String: AnyObject],
                            let msg = obj["welcomeMessageGeneral"] as? String
                        {
                            PopUpAlert.sharedInstance.showAlert(context: UIApplication.shared.keyWindow!.rootViewController!, title: "", message: msg )
                        }
                    })

                } else {
                    strongSelf.hideLoading()
                    strongSelf.logout(nil)
                }
                }, withCancel: { (error) in
                    print(error.localizedDescription)
            })
        } else {
            self.hideLoading()
            self.logout(nil)
        }
    }
    
    func updateUserDataInView()
    {
        if let ID = Auth.auth().currentUser?.uid
        {
            databaseRef.child("Users/\(ID)/name").observe(DataEventType.value, with: { snapshot in
                
                if let name = snapshot.value as? String
                {
                    self.userName.text = name
                    
                    if let profilePicturePath = snapshot.value as? String,
                        let url = URL(string: profilePicturePath)
                    {
                        self.profileImage.sd_setImage(with: url)
                    }
                }
            })
            
            
            databaseRef.child("Users/\(ID)/profile_picture").observe(DataEventType.value, with: { snapshot in
                
                if let profilePicturePath = snapshot.value as? String,
                    let url = URL(string: profilePicturePath)
                {
                    self.profileImage.sd_setImage(with: url)
                }
            })
        }
    }
    
    func showUserItemsOnly()
    {
        for btn in self.adminsOnlyMenuButtons
        {
            btn.isHidden = true
        }
        
        for btn in self.usersOnlyMenuButtons
        {
            btn.isHidden = false
        }
    }
    
    func showAdminItemsOnly()
    {
        for btn in self.usersOnlyMenuButtons
        {
            btn.isHidden = true
        }
        
        for btn in self.adminsOnlyMenuButtons
        {
            btn.isHidden = false
        }
    }
    
    @IBAction func openPrivateMessages(_ sender: UIButton)
    {
        self.openMenuViewController(with: "PrivateChatViewController")
    }
    
    @IBAction func contactUs(_ sender: UIButton)
    {
        self.openMenuViewController(with: "UserReportsViewController")
    }
    
    @IBAction func chatSettings(_ sender: UIButton)
    {
        self.openMenuViewController(with: "ChatSettingsViewController")
    }
    @IBAction func shareApp(_ sender: UIButton)
    {
        // text to share
        let text = "راديو فهد الكبيسي"
        
        // set up activity view controller
        let textToShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // present the view controller
        self.present(activityViewController, animated: true) {
            self.hideMenu()
        }
    }
    
    @IBAction func openAbout(_ sender: UIButton)
    {
        self.openMenuViewController(with: "AboutAppViewController")
    }
    
    @IBAction func openSettings(_ sender: UIButton)
    {
        self.openMenuViewController(with: "SettingsViewController")
    }
    
    @IBAction func openEditProfile(_ sender: UIButton)
    {
        self.openMenuViewController(with: "EditProfileViewController")
    }
    
    func openMenuViewController(with StoryboardID: String)
    {
        let nav: UINavigationController? = UIApplication.shared.delegate!.window!!.rootViewController as? UINavigationController;
        let viewController: SlideMenuMainViewController? = nav?.viewControllers[0] as? SlideMenuMainViewController
        let viewController2: UIViewController = (storyboard?.instantiateViewController(withIdentifier: StoryboardID))!
//        let contentNavController = UINavigationController(rootViewController: viewController2)
//        contentNavController.setNavigationBarHidden(true, animated: false)
        (viewController?.rootViewController as? UINavigationController)?.pushViewController(viewController2, animated: true);
        
        viewController?.hideLeftView(animated: true, completionHandler: { () -> Void in });
        viewController?.hideRightView(animated: true, completionHandler: { () -> Void in });
    }
    
    @IBAction func setCurrentSoundInfo()
    {
        TextPickerAlertViewController.showTextInputPopup(inside: self, maximumLength: 100, minimumLength: 2, title: "معلومات الصوتية الحالية", txtPlaceholder: "أدخل تفاصيل الصوتية") { (text) in
            
            if text != ""
            {
                databaseRef.child("GeneralSettings/sound_info").setValue(text, withCompletionBlock: { (error, dbRef) in
                    PopUpAlert.sharedInstance.showAlert(context: self, title: "تم الحفظ", message: "تم الحفظ بنجاح", dismissAction: {
                        self.navigationController?.popViewController(animated: true)
                    })
                })
            } else {
                let errorAlert = UIAlertController(title: "خطأ", message: "يرجى إدخال رسالة", preferredStyle: .alert)
                errorAlert.addAction(UIAlertAction(title: "تم", style: .cancel, handler: {
                    alert -> Void in
//                    self.present(alertController, animated: true, completion: nil)
                }))
                self.present(errorAlert, animated: true, completion: nil)
            }

        }
        self.hideMenu()
    }
    
    @IBAction func setWelcomeMessage()
    {
        let alert = UIAlertController(title: "اختر نوع الرسالة", message: "", preferredStyle: .actionSheet)
        let yesButton = UIAlertAction(title: "رسالة ترحيبية للجميع", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            alert.dismiss(animated: false, completion:nil)
            self.setWelcomeMsg(isPublic: true)})

        let noButton = UIAlertAction(title: "رسالة ترحيبية للأعضاء الجدد", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            alert.dismiss(animated: true, completion: nil)
            self.setWelcomeMsg(isPublic: false)
        })
        let cancelButton = UIAlertAction(title: "إلغاء", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            alert.dismiss(animated: true, completion: nil)
        })
        alert.addAction(yesButton)
        alert.addAction(noButton)
        alert.addAction(cancelButton)
        present(alert, animated: true)
        self.hideMenu()
    }
    
    func setWelcomeMsg(isPublic: Bool)
    {
        let message = isPublic ? "رسالة ترحيبية للجميع" : "رسالة ترحيبية للأعضاء الجدد"
        let key = isPublic ? "welcomeMessageGeneral" : "welcomeMessage"
        
        TextPickerAlertViewController.showTextInputPopup(inside: self, maximumLength: 100, minimumLength: 2, title: message, txtPlaceholder: "أدخل تفاصيل الرسالة") { (text) in

            if text != ""
            {
                databaseRef.child("GeneralSettings/\(key)").setValue(text, withCompletionBlock: { (error, dbRef) in
                    PopUpAlert.sharedInstance.showAlert(context: self, title: "تم الحفظ", message: "تم الحفظ بنجاح", dismissAction: {
                        self.navigationController?.popViewController(animated: true)
                    })
                })
            } else {
                let errorAlert = UIAlertController(title: "خطأ", message: "يرجى إدخال رسالة", preferredStyle: .alert)
                errorAlert.addAction(UIAlertAction(title: "تم", style: .cancel, handler: {
                    alert -> Void in
//                    self.present(alertController, animated: true, completion: nil)
                }))
                let cancelButton = UIAlertAction(title: "إلغاء", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                    errorAlert.dismiss(animated: true, completion: nil)
                })
                errorAlert.addAction(cancelButton)
                self.present(errorAlert, animated: true, completion: nil)
            }
        }
        
        self.hideMenu()
    }
    
    @IBAction func sendVideo()
    {
        TextPickerAlertViewController.showTextInputPopup(inside: self, maximumLength: 20, minimumLength: 5, title: "ارسال فيديو", txtPlaceholder: "أدخل فقط رابط الفيديو") { (text) in
            
            if text != ""
            {
                // CODE HERE TO SEND VIDEO IN A PUSH NOTIFICATION
                
                let URL = "https://fcm.googleapis.com/fcm/send"
                let headers: HTTPHeaders = ["Authorization": "key=AAAA7EBr7IE:APA91bFzTdcUixpqIW5SnGYayzhAEXDEhWq34ncs_GuEoWGpQuW7B-KUj53qsYArbEW9ngMw_87xc47Ooz_mDUwKug7nN95DtVGWQrN9kA1MvhgToeF4RSQbovVsQJQh1ynj2olDn638aD8InUaS13cKDbAX-pWGXg",
                                            "Content-Type": "application/json"]
                
                let data: [String: String] = ["vid": text]
                let notification: [String: String] = ["title": "تم استلام فيديو جديد"]
                
                let parameters: Parameters = [
                    "to": "/topics/\(generalTopic)",
                    "data": data,
                    "notification": notification
                ]
                
                self.showLoading()
                Alamofire.request(URL, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON(completionHandler: { (respo) in
                    self.hideLoading()
                    if respo.result.isSuccess,
                        let _ = (respo.result.value as? NSDictionary)?["message_id"]
                    {
                        PopUpAlert.sharedInstance.showAlert(context: self, title: "", message: "تم الإرسال بنجاح")
                    } else {
                        PopUpAlert.sharedInstance.showAlert(context: self, title: "", message: "فشل الإرسال، يرجى المحاولة مرة أخرى")
                    }
                })
                
                
            } else {
                let errorAlert = UIAlertController(title: "خطأ", message: "يرجى إدخال رابط", preferredStyle: .alert)
                errorAlert.addAction(UIAlertAction(title: "تم", style: .cancel, handler: {
                    alert -> Void in
//                    self.present(alertController, animated: true, completion: nil)
                }))
                let cancelButton = UIAlertAction(title: "إلغاء", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                    errorAlert.dismiss(animated: true, completion: nil)
                })
                errorAlert.addAction(cancelButton)
                self.present(errorAlert, animated: true, completion: nil)
            }

            
        }
        self.hideMenu()
    }
    
    @IBAction func sendPublicNotification()
    {
        TextPickerAlertViewController.showTextInputPopup(inside: self, maximumLength: 20, minimumLength: 10, title: "ارسال تنبيهات جماعية", txtPlaceholder: "أدخل رسالة التنبيه هنا") { (text) in
            if text != ""
            {
                let URL = "https://fcm.googleapis.com/fcm/send"
                let headers: HTTPHeaders = ["Authorization": "key=AAAA7EBr7IE:APA91bFzTdcUixpqIW5SnGYayzhAEXDEhWq34ncs_GuEoWGpQuW7B-KUj53qsYArbEW9ngMw_87xc47Ooz_mDUwKug7nN95DtVGWQrN9kA1MvhgToeF4RSQbovVsQJQh1ynj2olDn638aD8InUaS13cKDbAX-pWGXg",
                                            "Content-Type": "application/json"]
                
                let data: [String: String] = ["pass": text]
                let notification: [String: String] = ["title": text]
                
                let parameters: Parameters = [
                    "to": "/topics/\(generalTopic)",
                    "data": data,
                    "notification": notification
                ]
                
                self.showLoading()
                Alamofire.request( URL, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON(completionHandler: { (response) in
                    self.hideLoading()
                    if response.result.isSuccess,
                        let _ = (response.result.value as? NSDictionary)?["message_id"]
                    {
                        PopUpAlert.sharedInstance.showAlert(context: self, title: "", message: "تم الإرسال بنجاح")
                    } else {
                        PopUpAlert.sharedInstance.showAlert(context: self, title: "", message: "فشل الإرسال، يرجى المحاولة مرة أخرى")
                    }
                })
            } else {
                let errorAlert = UIAlertController(title: "خطأ", message: "يرجى إدخال رسالة", preferredStyle: .alert)
                errorAlert.addAction(UIAlertAction(title: "تم", style: .cancel, handler: {
                    alert -> Void in
//                    self.present(alertController, animated: true, completion: nil)
                }))
                let cancelButton = UIAlertAction(title: "إلغاء", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                    errorAlert.dismiss(animated: true, completion: nil)
                })
                errorAlert.addAction(cancelButton)
                self.present(errorAlert, animated: true, completion: nil)
            }

        }
        
        self.hideMenu()
    }
    
    @IBAction func makeAd(_ sender: UIButton)
    {
        self.openMenuViewController(with: "MakeAdViewController")
    }
    
    @IBAction func openReports(_ sender: UIButton)
    {
        self.openMenuViewController(with: "ReportsViewController")
    }
    
    @IBAction func removeChatHistory(_ sender: UIButton)
    {
        self.openMenuViewController(with: "RemoveChatViewController")
    }
    
    @IBAction func logout(_ sender: UIButton?)
    {
        try! Auth.auth().signOut()
        Messaging.messaging().shouldEstablishDirectChannel = false
        Database.database().goOffline()

        // FB logout..
        let loginManager = LoginManager()
        loginManager.logOut()

        self.twitterLogout()
        
        if let player = (UIApplication.shared.delegate as! AppDelegate).audioPlayer
        {
            if player.isPlaying
            {
                player.pause()
            }
        }

        
        if let nav = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController
        {
            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginIntroViewController") as! LoginIntroViewController
            nav.setViewControllers([viewController], animated: true)
        }
    }
    
    func twitterLogout()
    {
        let twitterSessionStore = TWTRTwitter.sharedInstance().sessionStore
        twitterSessionStore.reload()
        
        for case let session as TWTRSession in twitterSessionStore.existingUserSessions()
        {
            twitterSessionStore.logOutUserID(session.userID)
        }
        
        URLSession.shared.reset {}
        
        let cookieStore = HTTPCookieStorage.shared
        cookieStore.cookies?.forEach { cookieStore.deleteCookie($0) }
        
        UserDefaults.standard.synchronize()

    }
    
    func hideMenu()
    {
        let nav: UINavigationController? = UIApplication.shared.delegate!.window!!.rootViewController as? UINavigationController;
        let viewController: SlideMenuMainViewController? = nav?.viewControllers[0] as? SlideMenuMainViewController
        viewController?.hideLeftViewAnimated()
    }
}

extension MenuViewController: UITextFieldDelegate
{
    @objc func didChangeText(_ textField: UITextField)
    {
        if textField.maxLength > 0
        {
            if let text = textField.text,
            text.characters.count > textField.maxLength
            {
                textField.backgroundColor = UIColor.red.withAlphaComponent(0.3)
            } else {
                textField.backgroundColor = UIColor.white
            }
        }
    }
    
    
}

@IBDesignable
class LeftAlignedIconButton: UIButton {
    override func layoutSubviews() {
        super.layoutSubviews()
        contentHorizontalAlignment = .left
        let availableSpace = UIEdgeInsetsInsetRect(bounds, contentEdgeInsets)
        let availableWidth = availableSpace.width - imageEdgeInsets.right - (imageView?.frame.width ?? 0) - (titleLabel?.frame.width ?? 0)
        titleEdgeInsets = UIEdgeInsets(top: 0, left: availableWidth - 30, bottom: 0, right: 0)
    }
}

