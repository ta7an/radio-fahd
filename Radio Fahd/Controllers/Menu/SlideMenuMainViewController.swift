//
//  ViewController.swift
//  KuwaytNews
//
//  Created by Mohamed Altahan on 4/1/16.
//  Copyleft © 2016 Plexable. All lefts reserved.
//

import UIKit
import LGSideMenuController

class SlideMenuMainViewController: LGSideMenuController {
    
    
    var menuViewController: MenuViewController?
    var isAdmin = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func awakeFromNib() {
        
        // must call these 2 methods to setup menu..
        super.awakeFromNib()
        setupMenuDefaultProperties()
    }
    
    func setupMenuDefaultProperties()
    {
        leftViewAnimationDuration = 0.5;
        isLeftViewHidesOnTouch = true
        isLeftViewSwipeGestureEnabled = true
        //        leftViewStatusBarVisibleOptions = .onAll
        //        self.leftViewBackgroundBlurEffect = UIBlurEffect(style: .extraLight)
    }
    
}

