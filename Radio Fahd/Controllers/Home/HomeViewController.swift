//
//  HomeViewController.swift
//  Radio Fahd
//
//  Created by Mohamed Altahan on 1/17/18.
//  Copyright © 2018 Mohamed Altahan. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import FirebaseDatabase

class HomeViewController: ButtonBarPagerTabStripViewController
{

    override func viewDidLoad()
    {
        self.settings.style.selectedBarHeight = 4
        self.settings.style.selectedBarBackgroundColor = UIColor(red: 111.0/255.0, green: 111.0/255.0, blue: 113.0/255.0, alpha: 1.0)
        self.settings.style.buttonBarBackgroundColor = UIColor(red: 9.0/255.0, green: 9.0/255.0, blue: 9.0/255.0, alpha: 1.0)
        self.settings.style.buttonBarItemBackgroundColor = UIColor(red: 9.0/255.0, green: 9.0/255.0, blue: 9.0/255.0, alpha: 1.0)
        
        super.viewDidLoad()
        
        self.setupNavigationBar()
        let buttonItem = UIBarButtonItem(image: UIImage(named: "menu"), style: .plain, target: self, action: #selector(self.openMenu))
        buttonItem.tintColor = .white
        self.navigationItem.leftBarButtonItem  = buttonItem
    }
    
    func setupNavigationBar()
    {
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 9.0/255.0, green: 9.0/255.0, blue: 9.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationController?.navigationBar.isTranslucent = false
        
        navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedStringKey.foregroundColor : UIColor.white,
            NSAttributedStringKey.font : UIFont(name: "GESSTwoMedium-Medium", size: 18)!
        ]
    }
        
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func openMenu()
    {
        let nav: UINavigationController = UIApplication.shared.delegate?.window!!.rootViewController as! UINavigationController
        (nav.viewControllers[0] as! SlideMenuMainViewController).showLeftView(animated: true, completionHandler: nil)
    }
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController]
    {
        let listenVC = ListenViewController.makeListenViewController()
        let generalChatVC = GeneralChatViewController.makeGeneralChatViewController()
        return [listenVC, generalChatVC]
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
