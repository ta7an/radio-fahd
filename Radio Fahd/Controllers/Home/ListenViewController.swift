//
//  ListenViewController.swift
//  Radio Fahd
//
//  Created by Mohamed Altahan on 1/19/18.
//  Copyright © 2018 Mohamed Altahan. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import AVFoundation
import MediaPlayer
import GoogleMobileAds
import FirebaseDatabase

let StreamUrl = "https://streamer.radio.co/s1a22e655c/listen"

class ListenViewController: UIViewController, IndicatorInfoProvider {
    
    @IBOutlet weak var currentSoundInfoLbl: UILabel!
    @IBOutlet weak var bannerView: GADBannerView!
    @IBOutlet weak var MuteSoundButton: UIButton!
    @IBOutlet weak var playButton: UIButton!
    fileprivate var _refHandle: DatabaseHandle?
    
    static func makeListenViewController() -> ListenViewController
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyboard.instantiateViewController(withIdentifier: "ListenViewController") as! ListenViewController
        return newViewController
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo
    {
        return "استمع"
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        bannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        bannerView.rootViewController = self
        bannerView.load(GADRequest())

//        self.setNowPlayingInfo()
//        self.player = AVPlayer(url: URL(string: StreamUrl)!)
        _refHandle = databaseRef.child("GeneralSettings/sound_info").observe(.value, with: { (snapshot) in
            if let obj = snapshot.value as? String
            {
                self.currentSoundInfoLbl.text = obj
            }
        })
        
        self.playAudioStream(self.playButton)
    }

    deinit {
        if let refHandle = _refHandle
        {
            databaseRef.child("GeneralSettings/sound_info").removeObserver(withHandle: refHandle)
        }
    }
    
    @IBAction func playAudioStream(_ sender: UIButton)
    {
        if let player = (UIApplication.shared.delegate as! AppDelegate).audioPlayer
        {
            if player.isPlaying
            {
                player.pause()
            } else {
                do {
                    try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
                    print("AVAudioSession Category Playback OK")
                    do {
                        try AVAudioSession.sharedInstance().setActive(true)
                        print("AVAudioSession is Active")
                    } catch let error as NSError {
                        print(error.localizedDescription)
                    }
                } catch let error as NSError {
                    print(error.localizedDescription)
                }
                
                player.volume = 1.0
                player.rate = 1.0
                player.play()
            }
            sender.isSelected = !sender.isSelected
        }
    }
    
    @IBAction func muteButtonPressed(_ sender: UIButton)
    {
        sender.isSelected = !sender.isSelected

        if let player = (UIApplication.shared.delegate as! AppDelegate).audioPlayer
        {
            
            if player.volume == 0.0
            {
                player.volume = 1.0
            } else {
                player.volume = 0.0
            }
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setNowPlayingInfo()
    {
        let nowPlayingInfoCenter = MPNowPlayingInfoCenter.default()
        var nowPlayingInfo = nowPlayingInfoCenter.nowPlayingInfo ?? [String: Any]()
        
        let title = "راديو فهد الكبيسي"
        let album = "مباشر"
        let image = UIImage()
        let artwork = MPMediaItemArtwork(boundsSize: image.size, requestHandler: {  (_) -> UIImage in
            return image
        })
        
        nowPlayingInfo[MPMediaItemPropertyTitle] = title
        nowPlayingInfo[MPMediaItemPropertyAlbumTitle] = album
        nowPlayingInfo[MPMediaItemPropertyArtwork] = artwork
        
        nowPlayingInfoCenter.nowPlayingInfo = nowPlayingInfo
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension AVPlayer {
    var isPlaying: Bool {
        return rate != 0 && error == nil
    }
}

