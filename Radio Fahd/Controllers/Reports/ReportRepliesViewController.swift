//
//  ReportRepliesViewController.swift
//  Radio Fahd
//
//  Created by Mohamed Altahan on 1/27/18.
//  Copyright © 2018 Mohamed Altahan. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import GoogleMobileAds
import Firebase
import MaterialTextField
//import IHKeyboardAvoiding
import SDWebImage

@objc(ReportRepliesViewController)
class ReportRepliesViewController: UIViewController, IndicatorInfoProvider, UITableViewDataSource, UITableViewDelegate,
    UITextFieldDelegate, UINavigationControllerDelegate
{
    // Instance variables
    @IBOutlet weak var chatTableView: UITableView!
    @IBOutlet weak var textField: MFTextField!
    @IBOutlet weak var sendButton: UIButton!
    var ref: DatabaseReference!
    var reportPath: String!
    var messages: [DataSnapshot]! = []
    var msglength: NSNumber = 1000
    fileprivate var _refHandle: DatabaseHandle?
    var userInfoObj: [String : String]?
    
    @IBOutlet weak var clientTable: UITableView!
    let chatCellSettings = ChatCellSettings.getInstance()
    
    static func makeReportRepliesViewController(reportPath: String) -> ReportRepliesViewController
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyboard.instantiateViewController(withIdentifier: "ReportRepliesViewController") as! ReportRepliesViewController
        newViewController.reportPath = reportPath
        return newViewController
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo
    {
        return "تواصل"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        KeyboardAvoiding.avoidingView = self.view
        
        self.setUpBackButton()
        self.clientTable.register(UITableViewCell.self, forCellReuseIdentifier: "tableViewCell")
        
        configureDatabase()
        
        self.chatTableView.estimatedRowHeight = 60
        self.chatTableView.rowHeight = UITableViewAutomaticDimension
        
        chatCellSettings?.setSenderBubbleColorHex("F02032")
        chatCellSettings?.setReceiverBubbleColorHex("FFFFFF")
        chatCellSettings?.setSenderBubbleNameTextColorHex("FFFFFF")
        chatCellSettings?.setReceiverBubbleNameTextColorHex("FFFFFF")
        chatCellSettings?.setSenderBubbleMessageTextColorHex("FFFFFF")
        chatCellSettings?.setReceiverBubbleMessageTextColorHex("000000")
        chatCellSettings?.setSenderBubbleTimeTextColorHex("FFFFFF")
        chatCellSettings?.setReceiverBubbleTimeTextColorHex("FFFFFF")
        chatCellSettings?.setSenderBubbleFontWithSizeForName(UIFont.boldSystemFont(ofSize: 11))
        chatCellSettings?.setReceiverBubbleFontWithSizeForName(UIFont.boldSystemFont(ofSize: 11))
        chatCellSettings?.setSenderBubbleFontWithSizeForMessage(UIFont.systemFont(ofSize: 14))
        chatCellSettings?.setReceiverBubbleFontWithSizeForMessage(UIFont.systemFont(ofSize: 14))
        chatCellSettings?.setSenderBubbleFontWithSizeForTime(UIFont.systemFont(ofSize: 11))
        
        //        self.chatTableView.register(ChatTableViewCell.self, forCellReuseIdentifier: "chatSend")
        //        self.chatTableView.register(ChatTableViewCell.self, forCellReuseIdentifier: "chatReceive")
        
        //  Converted to Swift 4 by Swiftify v1.0.6577 - https://objectivec2swift.com/
        var nib = UINib(nibName: "ChatSendCell", bundle: nil)
        self.chatTableView.register(nib, forCellReuseIdentifier: "chatSend")
        nib = UINib(nibName: "ChatReceiveCell", bundle: nil)
        self.chatTableView.register(nib, forCellReuseIdentifier: "chatReceive")
        chatTableView.prefetchDataSource = self
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    // MARK: Chat
    deinit {
        if let refHandle = _refHandle  {
            self.ref.removeObserver(withHandle: refHandle)
        }
    }
    
    func configureDatabase() {
        ref = Database.database().reference().child(self.reportPath)
        // Listen for new messages in the Firebase database
        _refHandle = self.ref.observe(.childAdded, with: { [weak self] (snapshot) -> Void in
            guard let strongSelf = self else { return }
            strongSelf.messages.append(snapshot)
            strongSelf.clientTable.insertRows(at: [IndexPath(row: strongSelf.messages.count-1, section: 0)], with: .automatic)
            strongSelf.clientTable.scrollToRow(at: IndexPath(item:strongSelf.messages.count-1, section: 0), at: .bottom, animated: true)
        })
        
        if let ID = Auth.auth().currentUser?.uid
        {
            databaseRef.child("Users/\(ID)").observeSingleEvent(of: .value, with: { (snapshot) in
                if let obj = snapshot.value as? [String: Any]
                {
                    self.userInfoObj = [:]
                    self.userInfoObj!["id"] = obj["user_id"] as? String ?? ""
                    self.userInfoObj!["name"] = obj["name"] as? String ?? ""
                    self.userInfoObj!["photo_profile"] = obj["profile_picture"] as? String ?? ""
                }
            })
        }
    }
    
    // UITextViewDelegate protocol methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let text = textField.text else { return true }
        textField.text = ""
        view.endEditing(true)
        let millisecondsSince1970 = Date().timeIntervalSince1970 * 1000
        let data = ["message": text, "timeStamp": "\(millisecondsSince1970)"] as [String : Any]
        sendMessage(withData: data)
        return true
    }
    
    func sendMessage(withData data: [String: Any])
    {
        if self.userInfoObj != nil
        {
            var mdata = data
            let createdObj = self.ref.childByAutoId()
            mdata["userModel"] = self.userInfoObj!
            createdObj.setValue(mdata)
        }
    }
    
    @IBAction func didSendMessage(_ sender: UIButton) {
        _ = textFieldShouldReturn(textField)
    }
    
    func inviteFinished(withInvitations invitationIds: [String], error: Error?) {
        if let error = error {
            print("Failed: \(error.localizedDescription)")
        } else {
            print("Invitations sent")
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        
        let newLength = text.characters.count + string.characters.count - range.length
        return newLength <= self.msglength.intValue // Bool
    }
    
    // UITableViewDataSource protocol methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let messageSnapshot: DataSnapshot! = self.messages[indexPath.row]
        guard let message = messageSnapshot.value as? [String:Any] else { return self.clientTable.dequeueReusableCell(withIdentifier: "chatReceive", for: indexPath) }
        
        var isSender = false
        if let UID = Auth.auth().currentUser?.uid,
            let fetchedID = ((message["userModel"] as? [String:String])?["id"]),
            UID == fetchedID
        {
            isSender = true
        }
        let ID = isSender ? "chatSend" : "chatReceive"
        let cell = self.clientTable.dequeueReusableCell(withIdentifier: ID, for: indexPath) as! ChatTableViewCellXIB
        cell.backgroundColor = .clear
        cell.chatNameLabel.text = ((message["userModel"] as? [String:String])?["name"]) ?? ""
        cell.chatMessageLabel.text = (message["message"] as? String) ?? ""
        if let path = ((message["userModel"] as? [String:String])?["photo_profile"]),
            let url = URL(string: path)
        {
            cell.chatUserImage.sd_setImage(with: url)
        } else {
            cell.chatUserImage.image = UIImage(named: "splash_logo")
        }
        (cell.triangleView as! TriangleView).fillColor = isSender ? appRedColor : UIColor.white
        cell.triangleView.setNeedsDisplay()
        if let time = message["timeStamp"] as? String,
            let timeDouble = Double(time)
        {
            let date = Date(timeIntervalSince1970: timeDouble / 1000.0)
            let str = Date.timeAgoStringFromDate(date: date)
            cell.chatTimeLabel.text = str
        }

        //        if let photoURL = (message["userModel"] as? [String:String])?["photo_profile"],
        //        let URL = URL(string: photoURL),
        //            let data = try? Data(contentsOf: URL) {
        //            cell.imageView?.image = UIImage(data: data)
        //        }
        return cell
    }
    
    func showAlert(withTitle title: String, message: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title,
                                          message: message, preferredStyle: .alert)
            let dismissAction = UIAlertAction(title: "Dismiss", style: .destructive, handler: nil)
            alert.addAction(dismissAction)

            alert.view.tintColor = .black
            self.present(alert, animated: true, completion: {
                alert.view.tintColor = .black
            })
        }
    }
    
    
}

extension ReportRepliesViewController: UITableViewDataSourcePrefetching {
    public func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        

        let urls = indexPaths.map({ (indexPath) -> URL? in
            let messageSnapshot: DataSnapshot! = self.messages[indexPath.row]
            guard let message = messageSnapshot.value as? [String:Any] else {return nil}

            if let path = ((message["userModel"] as? [String:String])?["photo_profile"]),
                let url = URL(string: path)
            {
                return url
            }
            
            return nil

        })
        SDWebImagePrefetcher.shared().prefetchURLs(urls)
    }
}
