//
//  UserReportsViewController.swift
//  Radio Fahd
//
//  Created by Mohamed Altahan on 3/17/18.
//  Copyright © 2018 Mohamed Altahan. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth

class UserReportsViewController: UIViewController , UITableViewDataSource, UITableViewDelegate
{
    @IBOutlet weak var tableView: UITableView!
    var reports: [[String: Any]] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 80
        
        self.setUpBackButton()
        self.tableView.tableFooterView = UIView(frame: CGRect.zero);
        self.reloadData()
    }
    
    func reloadData()
    {
        if let UID = Auth.auth().currentUser?.uid
        {
            databaseRef.child("Reports/\(UID)").observeSingleEvent(of: .value, with: { (snapshot) in
                if let userReports = snapshot.value as? [String: [String: Any]]
                {
                    for (reportID, report) in userReports
                    {
                        var reportCopy: [String: Any] = [:]
                        for (key, value) in report
                        {
                            reportCopy[key] = value
                        }
                        reportCopy["userID"] = UID
                        reportCopy["reportID"] = reportID
                        self.reports.append(reportCopy)
                    }
                    
                    self.reports.reverse()
                    self.tableView.reloadData()
                }
            })
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reports.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let ID = "UserReportsTableViewCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: ID, for: indexPath) as! UITableViewCell
        
        let currentReport = self.reports[indexPath.row]
        cell.textLabel?.text = (currentReport["message"] as? String) ?? ""
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let report = reports[indexPath.row]

        if let UID = Auth.auth().currentUser?.uid,
            let reportID = report["reportID"] as? String
        {
            let replyVC = ReportRepliesViewController.makeReportRepliesViewController(reportPath: "Reports/\(UID)/\(reportID)/replies")
            self.navigationController?.pushViewController(replyVC, animated: true)
        }
    }
    
    @IBAction func makeReport()
    {
        if let VC = self.storyboard?.instantiateViewController(withIdentifier: "MakeReportViewController")
        {
            self.navigationController?.pushViewController(VC, animated: true)
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
