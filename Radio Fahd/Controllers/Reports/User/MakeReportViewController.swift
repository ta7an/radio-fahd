//
//  MakeReportViewController.swift
//  Radio Fahd
//
//  Created by Mohamed Altahan on 1/22/18.
//  Copyright © 2018 Mohamed Altahan. All rights reserved.
//

import UIKit
//import IHKeyboardAvoiding
import FirebaseAuth
import FirebaseDatabase

class MakeReportViewController: UIViewController, UITextViewDelegate {

    @IBOutlet weak var textView: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setUpBackButton()
//        KeyboardAvoiding.avoidingView = self.view
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        super.touchesBegan(touches, with: event)
    }

    @IBAction func sendReport(_ sender: UIButton)
    {
        self.view.endEditing(true)
        
        // send report..
        if let ID = Auth.auth().currentUser?.uid
        {
            databaseRef.child("Users/\(ID)").observeSingleEvent(of: .value, with: { (snapshot) in
                if let obj = snapshot.value as? [String: Any]
                {
                    let reportObj: [String : Any] = ["message": self.textView.text,
                                     "userInfo": obj]
                    databaseRef.child("Reports/\(ID)").childByAutoId().setValue(reportObj, withCompletionBlock: { (error, dbRef) in
                        if nil == error
                        {
                            PopUpAlert.sharedInstance.showAlert(context: self, title: "تم الإرسال", message: "تم إرسال شكواكم بنجاح، شكرا لك", dismissAction: {
                                self.navigationController?.popViewController(animated: true)
                            })
                        }
                    })
                }
            })
            
        }
//
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if textView.text.characters.count > 100
        {
            // show 100 by max chars alert..
            
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
