//
//  ReportsTableViewCell.swift
//  Radio Fahd
//
//  Created by Mohamed Altahan on 1/27/18.
//  Copyright © 2018 Mohamed Altahan. All rights reserved.
//

import UIKit

class ReportsTableViewCell: UITableViewCell {

    @IBOutlet weak var senderName: UILabel!
    @IBOutlet weak var message: UILabel!
    @IBOutlet weak var senderImage: UIImageView!
    @IBOutlet weak var optionsButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
