//
//  ReportsViewController.swift
//  Radio Fahd
//
//  Created by Mohamed Altahan on 1/27/18.
//  Copyright © 2018 Mohamed Altahan. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth
import SDWebImage

class ReportsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate
{
    @IBOutlet weak var tableView: UITableView!
    var reports: [[String: Any]] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 80
        
        self.setUpBackButton()
        self.tableView.tableFooterView = UIView(frame: CGRect.zero);
        self.reloadData()
    }

    func reloadData()
    {
        databaseRef.child("Reports").observeSingleEvent(of: .value, with: { (snapshot) in
            if let obj = snapshot.value as? [String: [String: [String: Any]]]
            {
                for (UserID, userReports) in obj
                {
                    for (reportID, report) in userReports
                    {
                        var reportCopy: [String: Any] = [:]
                        for (key, value) in report
                        {
                            reportCopy[key] = value
                        }
                        reportCopy["userID"] = UserID
                        reportCopy["reportID"] = reportID
                        self.reports.append(reportCopy)
                    }
                }
                
                self.reports.reverse()
                self.tableView.reloadData()
            }
        })

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reports.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let ID = "ReportsTableViewCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: ID, for: indexPath) as! ReportsTableViewCell

        let currentReport = self.reports[indexPath.row]
        cell.message.text = (currentReport["message"] as? String) ?? ""
        let userInfo = (currentReport["userInfo"] as? [String: Any])
        cell.senderName.text = (userInfo?["name"] as? String) ?? ""
        
        if let imagePath = userInfo?["profile_picture"]  as? String,
            let url = URL(string: imagePath)
        {
            cell.senderImage.sd_setImage(with: url)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }

    @IBAction func optionsButtonClicked(_ sender: UIButton)
    {
        let position = sender.convert(CGPoint.zero, to: self.tableView)
        if let indexPath = self.tableView.indexPathForRow(at: position)
        {
        let report = reports[indexPath.row]
        
            let alert = UIAlertController(title: "راديو فهد الكبيسي", message: "", preferredStyle: .actionSheet)
            let yesButton = UIAlertAction(title: "رد", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                alert.dismiss(animated: false, completion:nil)
                if let UID = report["userID"]  as? String,
                    let reportID = report["reportID"] as? String
                {
                    let replyVC = ReportRepliesViewController.makeReportRepliesViewController(reportPath: "Reports/\(UID)/\(reportID)/replies")
                    self.navigationController?.pushViewController(replyVC, animated: true)
                }
            })


            let noButton = UIAlertAction(title: "حذف", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                alert.dismiss(animated: true, completion: nil)
                
                databaseRef.child("Reports/\(report["userID"] as? String ?? "")/\(report["reportID"] as? String ?? "")").removeValue(completionBlock: { (error, ref) in
                    if nil == error
                    {
                        self.reloadData()
                    }
                })
            })
            let cancelButton = UIAlertAction(title: "إلغاء", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                alert.dismiss(animated: true, completion: nil)
            })
            alert.addAction(yesButton)
            alert.addAction(noButton)
            alert.addAction(cancelButton)
            alert.view.tintColor = .black
            present(alert, animated: true, completion: {
                alert.view.tintColor = .black
            })
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
