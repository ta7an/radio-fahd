//
//  SignUpViewController.swift
//  Radio Fahd
//
//  Created by Mohamed Altahan on 1/14/18.
//  Copyright © 2018 Mohamed Altahan. All rights reserved.
//

import UIKit
//import IHKeyboardAvoiding
import CountryPicker
import MaterialTextField
import FirebaseAuth
import SwiftValidator
import FirebaseMessaging
import Firebase
import FacebookLogin
import FBSDKLoginKit
import TwitterKit

class SignUpViewController: UIViewController, UITextFieldDelegate, ValidationDelegate, FBSDKLoginButtonDelegate
{
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        
    }
    
    
    var alertController: UIAlertController?
    @IBOutlet weak var twitterButton: TWTRLogInButton!
    @IBOutlet weak var FBButton: FBSDKLoginButton!
    @IBOutlet weak var scrollView: PassTouchesScrollView!
    @IBOutlet weak var nameTF: MFTextField!
    @IBOutlet weak var emailTF: MFTextField!
    @IBOutlet weak var countryTF: MFTextField!
    @IBOutlet weak var ageTF: MFTextField!
    @IBOutlet weak var passwordTF: MFTextField!
    let validator = Validator()

    
    override func viewDidLoad() {
        super.viewDidLoad()

//        KeyboardAvoiding.avoidingView = self.view
        self.setUpBackButton()
        self.scrollView.delegatePass = self

        validator.registerField(nameTF, rules: [RequiredRule(message: "يجب ملء الاسم")])
        validator.registerField(emailTF, rules: [RequiredRule(message: "يجب إدخال الإيميل"), EmailRule(message: "ايميل خاطئ")])
//        validator.registerField(ageTF, rules: [RequiredRule(message: "يجب إدخال السن")])
//        validator.registerField(countryTF, rules: [RequiredRule(message: "يجب إدخال الدولة")])
        validator.registerField(passwordTF, rules: [RequiredRule(message: "يجب إدخال الرقم السري")])
        FBButton.delegate = self
        
        twitterButton.logInCompletion = { session, error in
            if (session != nil) {
                let credential = TwitterAuthProvider.credential(withToken: session!.authToken, secret: session!.authTokenSecret)
                Auth.auth().signIn(with: credential) { (user, error) in
                    if let err = error {
                        PopUpAlert.sharedInstance.showAlert(context: self.navigationController!, title: "", message: "حدث خطأ ما، يرجآ المحاولة مرة أخرى")
                        return
                    }
                    let client = TWTRAPIClient.withCurrentUser()
                    
                    client.requestEmail { email, error in
                        let data: [String: Any] = ["\(Auth.auth().currentUser!.uid)":
                            ["name":session?.userName ?? "",
                             "FBM_Token": InstanceID.instanceID().token() ?? "",
                             "user_id": Auth.auth().currentUser!.uid,
                             "email": email ?? "",
                             "loginType": "tw" // "tw"
                            ]
                        ]
                        Messaging.messaging().shouldEstablishDirectChannel = true
                        Database.database().goOnline()

                        databaseRef.child("Users").updateChildValues(data)
                        
                        let sideMenuController = self.storyboard?.instantiateViewController(withIdentifier: "SlideMenuMainViewController") as! SlideMenuMainViewController
                        self.navigationController?.setNavigationBarHidden(true, animated: false)
                        self.navigationController?.setViewControllers([sideMenuController], animated: true)
                        databaseRef.child("GeneralSettings").observeSingleEvent(of: .value, with: { (snapshot) in
                            if let obj = snapshot.value as? [String: String],
                                let msg = obj["welcomeMessage"]
                            {
                                PopUpAlert.sharedInstance.showAlert(context: self.navigationController!, title: "", message: msg)
                            }
                        })
                    }
                }
            } else {
                PopUpAlert.sharedInstance.showAlert(context: self.navigationController!, title: "", message: "لم يتم الدخول عبر تويتر، يرجى المحاولة مرة أخرى والانتظار بعد الموافقة على الدخول عبر تويتر")
            }
        }

    }

    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        if nil == error {
            let credential = FacebookAuthProvider.credential(withAccessToken: FBSDKAccessToken.current().tokenString)
            Auth.auth().signIn(with: credential) { (user, er) in
                if let _ = er {
                    self.hideLoading()
                    PopUpAlert.sharedInstance.showAlert(context: self.navigationController!, title: "", message: "لم يتم الدخول عبر فيسبوك، يرجى المحاولة مرة أخرى والانتظار بعد الموافقة على الدخول عبر فيسبوك")
                    return
                }
                let data: [String: Any] = ["\(Auth.auth().currentUser!.uid)":
                    ["name":user?.displayName ?? "",
                     "FBM_Token": InstanceID.instanceID().token() ?? "",
                     "user_id": Auth.auth().currentUser!.uid,
                     "email": user?.email ?? "",
                     "loginType": "fb" // "tw"
                    ]
                ]
                Messaging.messaging().shouldEstablishDirectChannel = true
                Database.database().goOnline()

                databaseRef.child("Users").updateChildValues(data)
                
                let sideMenuController = self.storyboard?.instantiateViewController(withIdentifier: "SlideMenuMainViewController") as! SlideMenuMainViewController
                self.navigationController?.setNavigationBarHidden(true, animated: false)
                self.navigationController?.setViewControllers([sideMenuController], animated: true)
                databaseRef.child("GeneralSettings").observeSingleEvent(of: .value, with: { (snapshot) in
                    if let obj = snapshot.value as? [String: String],
                        let msg = obj["welcomeMessage"]
                    {
                        PopUpAlert.sharedInstance.showAlert(context: self.navigationController!, title: "", message: msg)
                    }
                })
                
            }
        } else {
            self.hideLoading()
            PopUpAlert.sharedInstance.showAlert(context: self.navigationController!, title: "", message: "لم يتم الدخول عبر فيسبوك، يرجى المحاولة مرة أخرى والانتظار بعد الموافقة على الدخول عبر فيسبوك")

        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        super.touchesBegan(touches, with: event)
    }

    func chooseCountry()
    {
        alertController = UIAlertController(title: "اختيار الدولة", message: "\n\n\n\n\n\n\n\n\n\n\n\n", preferredStyle: UIAlertControllerStyle.actionSheet)
        alertController?.view.tintColor = .black

        let margin:CGFloat = 8.0
        let rect = CGRect(x: margin, y: 0, width: alertController!.view.bounds.size.width - margin * 4.0, height: 240)
        let pickerView = CountryPicker(frame: rect)
        pickerView.delegate = self
        alertController?.view.addSubview(pickerView)
        
        let cancelAction = UIAlertAction(title: "إلغاء", style: .cancel, handler: {(alert: UIAlertAction!) in self.view.endEditing(true)})
        
        alertController?.addAction(cancelAction)
        
        DispatchQueue.main.async {
            self.present(self.alertController!, animated: true, completion:{
                self.alertController?.view.tintColor = .black
            })
        }
    }

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        if textField == self.countryTF
        {
            if textField.text != nil && textField.text!.characters.count > 0
            {
                return true
            }
            self.chooseCountry()
            return false
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        validator.validateField(textField){ error in
            if error == nil {
                
            } else {
                
            }
        }
        return true
    }

    
    @IBAction func createAccount(_ sender: AnyObject) {
        validator.validate(self)

    }
    
    func validationSuccessful()
    {
        
        if let email = emailTF.text,
            let password = passwordTF.text
        {
            self.showLoading()

            // [START create_user]
            Auth.auth().createUser(withEmail: email, password: password) { (user, error) in
                // [START_EXCLUDE]
                self.hideLoading()

                if let error = error {
                    PopUpAlert.sharedInstance.showAlert(context: self, title: "خطأ", message: error.localizedDescription)
                    return
                }
                print("\(user!.email!) created")
                
                let data: [String: Any] = ["\(Auth.auth().currentUser!.uid)":
                    ["name":self.nameTF.text!,
                     "FBM_Token": InstanceID.instanceID().token() ?? "",
                     "country": self.countryTF.text ?? "",
                     "user_id": Auth.auth().currentUser!.uid,
                     "age": self.ageTF.text ?? "",
                     "email": email,
                     "password": password]
                ]
                Messaging.messaging().shouldEstablishDirectChannel = true
                Database.database().goOnline()

                databaseRef.child("Users").updateChildValues(data)
                
                let sideMenuController = self.storyboard?.instantiateViewController(withIdentifier: "SlideMenuMainViewController") as! SlideMenuMainViewController
                self.navigationController?.setNavigationBarHidden(true, animated: false)
                self.navigationController?.setViewControllers([sideMenuController], animated: true)
                databaseRef.child("GeneralSettings").observeSingleEvent(of: .value, with: { (snapshot) in
                    if let obj = snapshot.value as? [String: String],
                        let msg = obj["welcomeMessage"]
                    {
                        PopUpAlert.sharedInstance.showAlert(context: self.navigationController!, title: "", message: msg)
                    }
                })


            }
            // [END create_user]
        }
    }

    func validationFailed(_ errors: [(Validatable, ValidationError)]) {
        // turn the fields to red
        for (field, error) in errors {
            if let field = field as? MFTextField {
//                field.layer.borderColor = UIColor.red.cgColor
//                field.layer.borderWidth = 1.0
                
                let err = NSError(domain: error.errorMessage, code: 100, userInfo: [NSLocalizedDescriptionKey: error.errorMessage])
                field.setError(err as! Error, animated: true)
            }
            error.errorLabel?.text = error.errorMessage // works if you added labels
            error.errorLabel?.isHidden = false
        }
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    

}

extension SignUpViewController: CountryPickerDelegate, PassTouchesScrollViewDelegate
{
    func countryPicker(_ picker: CountryPicker!, didSelectCountryWithName name: String!, code: String!)
    {
        self.countryTF.text = name
        self.alertController?.dismiss(animated: true, completion: nil)
    }
    
    func touchBegan()
    {
        self.view.endEditing(true)
    }
    func touchMoved() {}
}
