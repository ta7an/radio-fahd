//
//  LoginViewController.swift
//  Radio Fahd
//
//  Created by Mohamed Altahan on 1/14/18.
//  Copyright © 2018 Mohamed Altahan. All rights reserved.
//

import UIKit
import FirebaseAuth
import MaterialTextField
//import IHKeyboardAvoiding
import SwiftValidator
import FacebookLogin
import FBSDKLoginKit
import Firebase
import TwitterKit

class LoginViewController: UIViewController, ValidationDelegate, FBSDKLoginButtonDelegate
{
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        
    }
    

    @IBOutlet weak var FBButton: FBSDKLoginButton!
    @IBOutlet weak var emailTextField: MFTextField!
    @IBOutlet weak var passwordTextField: MFTextField!
    @IBOutlet weak var twitterButton: TWTRLogInButton!
    let validator = Validator()

    override func viewDidLoad() {
        super.viewDidLoad()
        
//        KeyboardAvoiding.avoidingView = self.view
        self.setUpBackButton()
        
        validator.registerField(emailTextField, rules: [RequiredRule(message: "يجب إدخال الإيميل"), EmailRule(message: "ايميل خاطئ")])
        validator.registerField(passwordTextField, rules: [RequiredRule(message: "يجب إدخال الرقم السري")])
        FBButton.delegate = self
        
        twitterButton.logInCompletion = { session, error in
            if (session != nil) {
                let credential = TwitterAuthProvider.credential(withToken: session!.authToken, secret: session!.authTokenSecret)
                Auth.auth().signIn(with: credential) { (user, error) in
                    if let err = error {
                        PopUpAlert.sharedInstance.showAlert(context: self.navigationController!, title: "", message: "حدث خطأ ما، يرجآ المحاولة مرة أخرى")
                        return
                    }
                    let client = TWTRAPIClient.withCurrentUser()

                    client.requestEmail { email, error in
                            let data: [String: Any] = ["\(Auth.auth().currentUser!.uid)":
                                ["name":session?.userName ?? "",
                                 "FBM_Token": InstanceID.instanceID().token() ?? "",
                                 "user_id": Auth.auth().currentUser!.uid,
                                 "email": email ?? "",
                                 "loginType": "tw" // "tw"
                                ]
                            ]
                        Messaging.messaging().shouldEstablishDirectChannel = true
                        Database.database().goOnline()

                            databaseRef.child("Users").updateChildValues(data)

                            let sideMenuController = self.storyboard?.instantiateViewController(withIdentifier: "SlideMenuMainViewController") as! SlideMenuMainViewController
                            self.navigationController?.setNavigationBarHidden(true, animated: false)
                            self.navigationController?.setViewControllers([sideMenuController], animated: true)
                            databaseRef.child("GeneralSettings").observeSingleEvent(of: .value, with: { (snapshot) in
                                if let obj = snapshot.value as? [String: String],
                                    let msg = obj["welcomeMessage"]
                                {
                                    PopUpAlert.sharedInstance.showAlert(context: self.navigationController!, title: "", message: msg)
                                }
                            })
                    }
                }
            } else {
                PopUpAlert.sharedInstance.showAlert(context: self.navigationController!, title: "", message: "لم يتم الدخول عبر تويتر، يرجى المحاولة مرة أخرى والانتظار بعد الموافقة على الدخول عبر تويتر")
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!)
    {
        self.showLoading()
        if error == nil
        {
            let credential = FacebookAuthProvider.credential(withAccessToken: FBSDKAccessToken.current().tokenString)
            Auth.auth().signIn(with: credential) { (user, er) in
                self.hideLoading()
                if let _ = er {
                    self.hideLoading()
                    PopUpAlert.sharedInstance.showAlert(context: self.navigationController!, title: "", message: "لم يتم الدخول عبر فيسبوك، يرجى المحاولة مرة أخرى والانتظار بعد الموافقة على الدخول عبر فيسبوك")
                    return
                }
                let data: [String: Any] = ["\(Auth.auth().currentUser!.uid)":
                    ["name":user?.displayName ?? "",
                     "FBM_Token": InstanceID.instanceID().token() ?? "",
                     "user_id": Auth.auth().currentUser!.uid,
                     "email": user?.email ?? "",
                     "loginType": "fb" // "tw"
                    ]
                ]
                Messaging.messaging().shouldEstablishDirectChannel = true
                Database.database().goOnline()

                databaseRef.child("Users").updateChildValues(data)
                
                let sideMenuController = self.storyboard?.instantiateViewController(withIdentifier: "SlideMenuMainViewController") as! SlideMenuMainViewController
                self.navigationController?.setNavigationBarHidden(true, animated: false)
                self.navigationController?.setViewControllers([sideMenuController], animated: true)
                databaseRef.child("GeneralSettings").observeSingleEvent(of: .value, with: { (snapshot) in
                    if let obj = snapshot.value as? [String: String],
                        let msg = obj["welcomeMessage"]
                    {
                        PopUpAlert.sharedInstance.showAlert(context: self.navigationController!, title: "", message: msg)
                    }
                })
                
            }
        } else {
            self.hideLoading()
            PopUpAlert.sharedInstance.showAlert(context: self.navigationController!, title: "", message: "لم يتم الدخول عبر فيسبوك، يرجى المحاولة مرة أخرى والانتظار بعد الموافقة على الدخول عبر فيسبوك")
        }
        
    }

    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        validator.validateField(textField){ error in
            if error == nil {
                
            } else {
                
            }
        }
        return true
    }
    
    
    @IBAction func loginAction(_ sender: UIButton)
    {
        validator.validate(self)
    }

    func validationSuccessful()
    {
        if self.emailTextField.text == "" || self.passwordTextField.text == "" {
            
            //Alert to tell the user that there was an error because they didn't fill anything in the textfields because they didn't fill anything in
            
            let alertController = UIAlertController(title: "خطأ", message: "يرجى إدخال بيانات دخول صحيحة", preferredStyle: .alert)
            
            let defaultAction = UIAlertAction(title: "تم", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            alertController.view.tintColor = .black

            self.present(alertController, animated: true, completion: {
                alertController.view.tintColor = .black
            })
            
        } else {
            self.showLoading()

            Auth.auth().signIn(withEmail: self.emailTextField.text!, password: self.passwordTextField.text!) { (user, error) in
                self.hideLoading()
                if error == nil {
                    
                    Messaging.messaging().shouldEstablishDirectChannel = true
                    Database.database().goOnline()

                    //Print into the console if successfully logged in
                    print("You have successfully logged in")
                    
                    databaseRef.child("Users/\(Auth.auth().currentUser!.uid)/FBM_Token").setValue(InstanceID.instanceID().token() ?? "")

                    
                    //Go to the HomeViewController if the login is sucessful
                    let sideMenuController = self.storyboard?.instantiateViewController(withIdentifier: "SlideMenuMainViewController") as! SlideMenuMainViewController
                    self.navigationController?.setNavigationBarHidden(true, animated: false)
                    self.navigationController?.setViewControllers([sideMenuController], animated: true)
                    databaseRef.child("GeneralSettings").observeSingleEvent(of: .value, with: { (snapshot) in
                        if let obj = snapshot.value as? [String: String],
                            let msg = obj["welcomeMessage"]
                        {
                            PopUpAlert.sharedInstance.showAlert(context: self.navigationController!, title: "", message: msg)
                        }
                    })


                } else {
                    
                    //Tells the user that there is an error and then gets firebase to tell them the error
                    let alertController = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .alert)
                    
                    let defaultAction = UIAlertAction(title: "تم", style: .cancel, handler: nil)
                    alertController.addAction(defaultAction)
                    alertController.view.tintColor = .black
                    self.present(alertController, animated: true, completion: {
                        alertController.view.tintColor = .black
                    })
                }
            }
        }
    }

    func validationFailed(_ errors: [(Validatable, ValidationError)]) {
        // turn the fields to red
        for (field, error) in errors {
            if let field = field as? MFTextField {
//                field.layer.borderColor = UIColor.red.cgColor
//                field.layer.borderWidth = 1.0
                
                let err = NSError(domain: error.errorMessage, code: 100, userInfo: [NSLocalizedDescriptionKey: error.errorMessage])
                field.setError(err as! Error, animated: true)
            }
            error.errorLabel?.text = error.errorMessage // works if you added labels
            error.errorLabel?.isHidden = false
        }
    }


    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        super.touchesBegan(touches, with: event)
    }
    
    @IBAction func reserPassword(_ sender: UIButton)
    {
//        Auth.auth().verifyPasswordResetCode(<#T##code: String##String#>, completion: <#T##VerifyPasswordResetCodeCallback##VerifyPasswordResetCodeCallback##(String?, Error?) -> Void#>)

    }
//    @IBAction func twitterButtonPressed(_ sender: UIButton) {
////        TWTRTwitter.sharedInstance().auth
//        TWTRTwitter.sharedInstance().logIn() { session, error in
//            if (session != nil) {
//                let credential = TwitterAuthProvider.credential(withToken: session!.authToken, secret: session!.authTokenSecret)
//                Auth.auth().signIn(with: credential) { (user, error) in
//                    if let error = error {
//                        // ...
//                        return
//                    }
//                    let client = TWTRAPIClient.withCurrentUser()
//
//                    client.requestEmail { email, error in
//                        if (email != nil) {
//                            let data: [String: Any] = ["\(Auth.auth().currentUser!.uid)":
//                                ["name":session?.userName ?? "",
//                                 "FBM_Token": InstanceID.instanceID().token() ?? "",
//                                 "user_id": Auth.auth().currentUser!.uid,
//                                 "email": email ?? "",
//                                 "loginType": "tw" // "tw"
//                                ]
//                            ]
//                            databaseRef.child("Users").updateChildValues(data)
//
//                            let sideMenuController = self.storyboard?.instantiateViewController(withIdentifier: "SlideMenuMainViewController") as! SlideMenuMainViewController
//                            self.navigationController?.setNavigationBarHidden(true, animated: false)
//                            self.navigationController?.setViewControllers([sideMenuController], animated: true)
//                            databaseRef.child("GeneralSettings").observeSingleEvent(of: .value, with: { (snapshot) in
//                                if let obj = snapshot.value as? [String: String],
//                                    let msg = obj["welcomeMessage"]
//                                {
//                                    PopUpAlert.sharedInstance.showAlert(context: self.navigationController!, title: "", message: msg)
//                                }
//                            })
//
//                        } else {
//                        }
//                    }
//                }
//            } else {
//
//            }
//        }
//    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
