//
//  SendVerificationCodeViewController.swift
//  Radio Fahd
//
//  Created by Mohamed Altahan on 1/30/18.
//  Copyright © 2018 Mohamed Altahan. All rights reserved.
//

import UIKit
import MaterialTextField
import Firebase
import SwiftValidator

class SendVerificationCodeViewController: UIViewController, ValidationDelegate {

    @IBOutlet weak var codeTextField: MFTextField!
    let validator = Validator()

    override func viewDidLoad() {
        super.viewDidLoad()

        validator.registerField(codeTextField, rules: [RequiredRule(message: "يجب ملئ الكود")])
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func confirmCode(_ sender: UIButton)
    {
        validator.validate(self)
    }
    
    func validationSuccessful()
    {
        self.showLoading()
        Auth.auth().verifyPasswordResetCode(self.codeTextField.text!) { (code, error) in
            if error == nil
            {
                // Success
                self.performSegue(withIdentifier: "OpenChangePasswordScreen", sender: self.codeTextField.text!)
            }
        }
    }
    
    func validationFailed(_ errors: [(Validatable, ValidationError)])
    {
        
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let ID = segue.identifier,
        ID == "OpenChangePasswordScreen"
        {
            let dest = segue.destination as! ChangePasswordViewController
            dest.confirmCode = self.codeTextField.text!
        }
    }

}
