//
//  ChangePasswordViewController.swift
//  Radio Fahd
//
//  Created by Mohamed Altahan on 1/30/18.
//  Copyright © 2018 Mohamed Altahan. All rights reserved.
//

import UIKit
import SwiftValidator
import Firebase
import MaterialTextField

class ChangePasswordViewController: UIViewController, ValidationDelegate
{
    let validator = Validator()
    var confirmCode: String!

    @IBOutlet weak var passwordTF: MFTextField!
    @IBOutlet weak var confirmPasswordTF: MFTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        validator.registerField(self.passwordTF, rules: [RequiredRule(message: "يجب ملئ كلمة السر")])
        validator.registerField(self.confirmPasswordTF, rules: [ConfirmationRule(confirmField: self.passwordTF, message: "يجب أن تتطابق كلا من حقلي كلمة السر")])
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func changePassord(_ sender: UIButton)
    {
        validator.validate(self)
    }
    
    func validationSuccessful()
    {
        self.showLoading()
        Auth.auth().confirmPasswordReset(withCode: self.confirmCode, newPassword: self.passwordTF.text!) { (error) in
            if let nav = self.navigationController
            {
                PopUpAlert.sharedInstance.showAlert(context: self, title: "", message: "", dismissAction: {
                    let loginIntroVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginIntroViewController")
                    let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController")
                    nav.setViewControllers([loginIntroVC!, loginVC!], animated: true)
                })

            }
        }
    }
    
    func validationFailed(_ errors: [(Validatable, ValidationError)])
    {
        
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
