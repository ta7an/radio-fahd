//
//  ForgotPasswordViewController.swift
//  Radio Fahd
//
//  Created by Mohamed Altahan on 1/30/18.
//  Copyright © 2018 Mohamed Altahan. All rights reserved.
//

import UIKit
//import IHKeyboardAvoiding
import SwiftValidator
import Firebase
import MaterialTextField

class ForgotPasswordViewController: UIViewController, ValidationDelegate
{
    let validator = Validator()
    @IBOutlet weak var emailTextField: MFTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        KeyboardAvoiding.avoidingView = self.view
        validator.registerField(emailTextField, rules: [RequiredRule(), EmailRule(message: "ايميل خاطئ")])
        self.setUpBackButton()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func sendResetPasswordEmail(_ sender: UIButton)
    {
        validator.validate(self)
    }
    
    func validationSuccessful()
    {
        self.showLoading()
        Auth.auth().sendPasswordReset(withEmail: self.emailTextField.text!)
        { (error) in
            self.hideLoading()
            if nil == error
            {
                PopUpAlert.sharedInstance.showAlert(context: self, title: "تم", message: "يرجى التحقق من الايميل", dismissAction: {
                    self.navigationController?.popViewController(animated: true)
                })
            }
        }
    }
    
    func validationFailed(_ errors: [(Validatable, ValidationError)])
    {
    
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        super.touchesBegan(touches, with: event)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
