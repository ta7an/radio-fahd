//
//  UILabel+WholeAppFont.h
//  Pearl limousine
//
//  Created by Mohamed Altahan on 8/8/17.
//  Copyright © 2017 Mohamed Altahan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (WholeAppFont)
- (void)setSubstituteFontName:(NSString *)name UI_APPEARANCE_SELECTOR;

@end
