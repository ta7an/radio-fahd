//
//  UILabel+WholeAppFont.m
//  Pearl limousine
//
//  Created by Mohamed Altahan on 8/8/17.
//  Copyright © 2017 Mohamed Altahan. All rights reserved.
//

#import "UILabel+WholeAppFont.h"

@implementation UILabel (WholeAppFont)
- (void)setSubstituteFontName:(NSString *)name UI_APPEARANCE_SELECTOR
{
    self.font = [UIFont fontWithName:name size:self.font.pointSize];
}
    
    
    // SWIFT CODE TO GET FONT NAMES IN FAMILY
//    for name in UIFont.fontNames(forFamilyName: "GE SS Two")
//    {
//        print("  \(name)");
//    }

@end

