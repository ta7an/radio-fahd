//
//  TriangleView.swift
//  Radio Fahd
//
//  Created by Mohamed Altahan on 1/22/18.
//  Copyright © 2018 Mohamed Altahan. All rights reserved.
//

import UIKit

class TriangleView : UIView {
    var fillColor: UIColor = .white
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func draw(_ rect: CGRect) {
        
        guard let context = UIGraphicsGetCurrentContext() else { return }
        
        context.beginPath()
        context.move(to: CGPoint(x: rect.minX, y: rect.maxY))
        context.addLine(to: CGPoint(x: rect.maxX, y: rect.maxY))
        context.addLine(to: CGPoint(x: rect.maxX / 2.0, y: rect.minY))
        context.closePath()
        
        context.setFillColor(self.fillColor.cgColor)
        context.fillPath()
    }
}

