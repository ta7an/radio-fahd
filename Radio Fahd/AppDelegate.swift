//
//  AppDelegate.swift
//  Radio Fahd
//
//  Created by Mohamed Altahan on 3/14/17.
//  Copyright © 2017 Mohamed Altahan. All rights reserved.
//

import UIKit
import GoogleMobileAds
import Firebase
import FirebaseAuth
import IQKeyboardManagerSwift
import UserNotifications
import FacebookLogin
import FacebookCore
import SafariServices
import Reachability
import Toast_Swift
import TwitterKit
import PopupDialog

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate
{

    var window: UIWindow?
    let reachability = Reachability()!
    var audioPlayer: AVPlayer!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        UIApplication.shared.statusBarStyle = .lightContent
        UILabel.appearance().setSubstituteFontName("GESSTwoMedium-Medium")
        UIApplication.shared.statusBarStyle = .lightContent
        if let nav = self.window?.rootViewController as? UINavigationController
        {
            nav.setNavigationBarHidden(true, animated: false)
        }
        GADMobileAds.configure(withApplicationID: "ca-app-pub-3940256099942544/2934735716")
        application.beginReceivingRemoteControlEvents()

        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        Messaging.messaging().shouldEstablishDirectChannel = true
        Messaging.messaging().subscribe(toTopic: "global");

        
        IQKeyboardManager.sharedManager().enable = true
        self.startMonitoringNetworkConnectionStatus()
        
        SDKApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        TWTRTwitter.sharedInstance().start(withConsumerKey:"uuv8Vg97Q1p0dVWHNRO3Ic9an", consumerSecret:"EhP9hpGC9WZx4G7DicsFI4ibb6NeZmrFuIf3oYZKAruRiNEBni")

        self.audioPlayer = AVPlayer(url: URL(string: StreamUrl)!)

        self.setupAppearanceForAlertController()
        return true
    }
    func setupAppearanceForAlertController() {
        let view = UIView.appearance(whenContainedInInstancesOf: [UIAlertController.self])
        view.tintColor = .black
//        UIView.appearance().tintColor = .black
        UIButton.appearance().tintColor = .white
    }

    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        if let scheme = url.scheme,
            scheme.lowercased() == "twitterkit-uuv8Vg97Q1p0dVWHNRO3Ic9an".lowercased()
        {
            return TWTRTwitter.sharedInstance().application(app, open: url, options:options)
        }
            
        return SDKApplicationDelegate.shared.application(app, open:url, options:options)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
//        if let messageID = userInfo[MessageIDKey] {
//            print("Message ID: \(messageID)")
//        }
        
        // Print full message.
        print(userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
//        if let messageID = userInfo[gcmMessageIDKey] {
//            print("Message ID: \(messageID)")
//        }
        
        // Print full message.
        print(userInfo)
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}


extension AppDelegate: MessagingDelegate, SFSafariViewControllerDelegate
{
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        Messaging.messaging().subscribe(toTopic: "global");
        
        UserDefaults.standard.set( true, forKey: "NotificationsOn")

    }
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        let data = remoteMessage.appData
        if let type = data["type"] as? String
        {
            // Ad:
            if type == "ia"
            {
                // big add in app..
                if let bigAdVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "BigAdViewController") as? BigAdViewController
                {
                    bigAdVC.imagePath = data["image"] as? String
                    bigAdVC.adPath = data["url"] as? String
                    UIApplication.shared.keyWindow?.addSubview(bigAdVC.view)
                    UIApplication.shared.keyWindow?.rootViewController?.addChildViewController(bigAdVC)
                }
            } else if type == "nI"
            {
                // ad on top of screen..
                if let adPath = data["url"] as? String,
                    let url = URL(string: adPath),
                    UIApplication.shared.canOpenURL(url)
                {
                    UIApplication.shared.openURL(url)
                }

            }
//            if let controller =
        } else if let videoID = data["vid"] as? String
        {
//            let videoURL = "https://www.youtube.com/watch?v=\(videoID)"
//
//            show alert..
            let messageAlert = UIAlertController(title: "فيديو جديد", message: "شاهد الفيديو التالي من  فهد الكبيسي", preferredStyle: .alert)
            messageAlert.view.tintColor = .black

            messageAlert.addAction(UIAlertAction(title: "مشاهدة", style: .default, handler: {
                alert -> Void in
                // Show video..
                if let videoAdVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "VideoPLayerViewController") as? VideoPLayerViewController
                {
                    videoAdVC.videoID = videoID
                    UIApplication.shared.keyWindow?.addSubview(videoAdVC.view)
                    UIApplication.shared.keyWindow?.rootViewController?.addChildViewController(videoAdVC)
                }

//                if let controller = UIApplication.shared.keyWindow?.rootViewController,
//                    let url = URL(string: videoURL)
//                {
//                    let safariVC = SFSafariViewController(url: url)
//                    safariVC.delegate = self
//                    controller.present(safariVC, animated: true, completion: nil)
//                }
            }))
            messageAlert.addAction(UIAlertAction(title: "إخفاء", style: .cancel, handler: {
                alert -> Void in
                messageAlert.dismiss(animated: true, completion: nil)
            }))
            UIApplication.shared.keyWindow?.rootViewController?.present(messageAlert, animated: true, completion: {
                messageAlert.view.tintColor = .black
            })
        } else if let message = data["message"] as? String
        {
            let messageAlert = UIAlertController(title: "", message: message, preferredStyle: .alert)
            messageAlert.view.tintColor = .black
            messageAlert.addAction(UIAlertAction(title: "تم", style: .cancel, handler: {
                alert -> Void in
                messageAlert.dismiss(animated: true, completion: nil)
            }))
            UIApplication.shared.keyWindow?.rootViewController?.present(messageAlert, animated: true, completion: {
                messageAlert.view.tintColor = .black
            })
        }
        
    }
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController)
    {
        controller.dismiss(animated: true, completion: nil)
    }
}

//Mark: Network Listener
extension AppDelegate
{
    func startMonitoringNetworkConnectionStatus()
    {
        ToastManager.shared.isTapToDismissEnabled = false

        reachability.whenReachable = { reachability in
            print("Reachable")
            UIApplication.shared.keyWindow?.hideAllToasts()
        }
        reachability.whenUnreachable = { _ in
            print("Not reachable")
            UIApplication.shared.keyWindow?.makeToast("لا يوجد اتصال بالانترنت    ", duration: TimeInterval.greatestFiniteMagnitude, position: ToastPosition.bottom, title: nil, image: nil, style: ToastStyle(), completion: nil)
        }
        
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }

    }
}
