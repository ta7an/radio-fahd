//
//  AlertDialog.swift
//  WaveInsure
//
//  Created by MohamedKhalifa on 11/19/15.
//  Copyright © 2015 ITGSolutions. All rights reserved.
//

import Foundation
import UIKit
import PopupDialog

@objc public class PopUpAlert : NSObject
{
    
    var context: UIViewController?
    static var sharedInstance = PopUpAlert()
    
    
    
    func showAlert( context : UIViewController,title : String ,message : String, dismissAction: (() -> Void)? = nil)
    {
        DispatchQueue.main.async {
            
            self.context = context
            //        let alertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
            //
            //        let OKAction = UIAlertAction(title: "OK", style: .Default) { (action:UIAlertAction!) in
            //
            //        }
            //        alertController.addAction(OKAction)
            //
            //        context.presentViewController(alertController, animated: true, completion:nil)
            
            
            
            // Create the dialog
            
            let popup = PopupDialog(title: title, message: message, image: nil)
            popup.viewController.view.tintColor = .black
            
            // Create buttons
            let ok =  "تم"
            
            let buttonOne = DefaultButton(title: ok, action: dismissAction)
            buttonOne.tintColor = .black
            buttonOne.setTitleColor(.black, for: .normal)

            // Add buttons to dialog
            // Alternatively, you can use popup.addButton(buttonOne)
            // to add a single button
            popup.addButtons([buttonOne])
            
            // Present dialog
            context.present(popup, animated: true, completion: nil)
        }
    }
    
    func showAlert( context : UIViewController,title : String ,message : String,buttonTitle:String, dismissAction: (() -> Void)? = nil)
    {
        DispatchQueue.main.async {
            
            self.context = context
            //        let alertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
            //
            //        let OKAction = UIAlertAction(title: "OK", style: .Default) { (action:UIAlertAction!) in
            //
            //        }
            //        alertController.addAction(OKAction)
            //
            //        context.presentViewController(alertController, animated: true, completion:nil)
            
            
            
            // Create the dialog
            
            let popup = PopupDialog(title: title, message: message, image: nil)
            
            // Create buttons
            let buttonOne = DefaultButton(title: buttonTitle, action: dismissAction)
            
            
            // Add buttons to dialog
            // Alternatively, you can use popup.addButton(buttonOne)
            // to add a single button
            popup.addButtons([buttonOne])
            
            // Present dialog
            context.present(popup, animated: true, completion: nil)
            
        }
    }
    
    func showAlert( context : UIViewController,title : String ,message : String,buttonDismissTitle:String,buttonDoneTitle:String, dismissAction: (() -> Void)? = nil,  doneAction: (() -> Void)? = nil)
    {
        DispatchQueue.main.async {
            
            self.context = context
            //        let alertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
            //
            //        let OKAction = UIAlertAction(title: "OK", style: .Default) { (action:UIAlertAction!) in
            //
            //        }
            //        alertController.addAction(OKAction)
            //
            //        context.presentViewController(alertController, animated: true, completion:nil)
            
            
            
            // Create the dialog
            
            //   let popup = PopupDialog(title: title, message: message, image: nil)
            
            let popup = PopupDialog(title: title, message: message, image: nil, buttonAlignment:UILayoutConstraintAxis.horizontal, transitionStyle: PopupDialogTransitionStyle.bounceUp, gestureDismissal: true, completion: nil)
            // Create buttons
            let buttonOne = DefaultButton(title: buttonDismissTitle, action: dismissAction)
            
            let buttonTwo = DefaultButton(title: buttonDoneTitle, action: doneAction)
            
            
            // Add buttons to dialog
            // Alternatively, you can use popup.addButton(buttonOne)
            // to add a single button
            popup.addButtons([buttonOne,buttonTwo])
            
            // Present dialog
            context.present(popup, animated: true, completion: nil)
        }
    }
    
    
}
